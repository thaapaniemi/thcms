<?php

/**
* TCHMS Index page
* @package THCMS
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

session_start();

try{

require_once("config.php");

$pc = $GLOBALS['pageController'];
$login = $GLOBALS['loginController'];
$pi = $GLOBALS['pageInterface'];

$page= $GLOBALS['page'];
#$page1 = null;

$action = $GLOBALS['action'];

$login->handleLogin();

if(isset($_GET['page'])){
	$page=$_GET['page'];
#	$page1 = TPage::open($page, false);
}

#Action chooser, input actions handling
if(isset($_GET['action'])){
	$action=$_GET['action'];
	
	if($action=='logout'){
		$login->logout();
		header("Location: index.php");
	}else if($action == "login"){
		if(!$login->isLogged()){}
	}else if($action == "write" && $login->isLogged()){
		if(isset($_POST['pageedit'])){
			$parse = null;
			if(isset($_POST['parse'])){
				$parse = true;
			}else{
				$parse = false;
			}
			$pc->updatePage($page, $_POST['pageedit'], $parse, $_POST['group']);
			header("Location: index.php?page=". $page);
	}
	}
	
}


THTP::addReplace('<!-- THCMS:NAVBAR_UL_RIGHT -->', $pi->printPageTools($page) . $pageInterface->printNavbar(ADMINBAR) . $pi->navbarLoginTools() );
THTP::addReplace('<!-- THCMS:NAVBAR_RIGHT -->', $pi->navbarOpenIDLoginForm() . "\n" . $pi->navbarLoginForm() );


THTP::addReplace('<!-- THCMS:SIDEBAR -->', $pageInterface->readPage(SIDEBAR, true));

$parse = PageCache::getPage($page)->parse();
#Action chooser, output actions
if($action == "read"){
	THTP::addReplace('<!-- THCMS:CONTENT -->', $pi->readPage($page, $parse));
}
else if($action == "edit" && $login->isLogged()){
	if(!TPage::exists($page)){
		$pc->createPage($page);
	}
	THTP::addReplace('<!-- THCMS:CONTENT -->', $pi->printEditPageForm($page));
	
}else if($action == "create" && $login->isLogged()){
	$pc->createPage($page);
	THTP::addReplace('<!-- THCMS:CONTENT -->', $pi->readPage($page, $parse));
}else{
	THTP::addReplace('<!-- THCMS:CONTENT -->', $pi->readPage($page, $parse));
}


THTP::addReplace('<!-- THCMS:TITLE -->', CMSTITLE);
THTP::addReplace('<!-- THCMS:MENUTITLE -->', CMSTITLE);
THTP::addReplace('<!-- THCMS:NAVBAR_LEFT -->', $pageInterface->printNavbar(NAVBAR) . $pageInterface->printNavbar(NAVBAR_HIDDEN));
THTP::addReplace('<!-- THCMS:PAGELASTMODIFIED -->',$pageInterface->printPageLastModified($page));
THTP::addReplace('<!-- THCMS:POWEREDBY -->', $pageInterface->printPoweredByText());


}catch(Exception $e){
	THTP::load('<html><body><!-- THCMS:CONTENT --></body></html>');
	THTP::addReplace('<!-- THCMS:CONTENT -->', $e->getMessage());
}

echo THTP::getHTML();
?>
