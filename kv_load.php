<?php

/**
* TCHMS Key-value-DB file getter
* @package THCMS/KV
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

session_start();
ob_start();

require_once("config.php");
require_once("core/KV.php");

ob_end_clean();

$value = null;
if(isset($_GET['key'])){
	$value = (array) KV::get($_GET['key']);
}

if(isset($value)){
	if($value['valueType']== 'file'){
		$content = base64_decode($value['content']);
		#$content = $value['content'];
		header('Content-Type: ' . $value['contentType']);
		header('Content-Length: ' . $value['contentLength'] );
		echo $content;
	}else{
		print_r($value);
	}
}?>
