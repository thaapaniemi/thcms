<?php

/**
* TCHMS configuration file
* @package THCMS
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
* Edit this for your needs. Here should be most common configuration data for thcms.
*/

define('CMSTITLE','CMSTest'); // Page Title, seen in page title and in navbar
define('NAVBAR','Site.menubar'); // Navbar page name in DB
define('NAVBAR_HIDDEN','Site.hiddenNavbar'); // Navbar page name in DB
define('ADMINBAR','Site.adminbar'); // Navbar page name in DB
define('SIDEBAR','Site.sidebar'); // Sidebar page name in DB
define('MAINPAGE','Main'); // Main page name
define('ADMINPAGE','Admin'); // Admin interface page name

//DB Table names. This affects to used DB
define('PAGECLASS','thcms_pages');
define('USERCLASS','thcms_users');
define('GROUPCLASS','thcms_groups');

define('DOMAIN', $_SERVER['HTTP_HOST']);
#define('DOMAIN', $_SERVER['SERVER_ADDR']);
#define('DOMAIN', '192.168.100.250');

// Template in use, templates in directory templates/templatename
$template = "twbs";

//Idiorm DB configuration
$idiorm_statements = array(
#	'pgsql:host=localhost;dbname=my_database',
	'mysql:host=localhost;dbname=my_database',
#	'sqlite:'.$basedir.'/testdb/example.db',
	array('username' => 'database_user'),
	array('password' => 'top_secret')
);

#defaults
$action = "read";
$page = MAINPAGE;


// Here be all other configuration crap
require_once("config_internal.php");

?>
