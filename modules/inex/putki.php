<?php

class Putki{

	public $numero = null;
	public $tehty = null;
	public $kesto = null;
	public $id = null;
		
	public function __construct($numero, $tehty, $kesto, $id=null){
		$this->numero = $numero;
		$this->tehty = $tehty;
		$this->kesto = $kesto;
		$this->id = $id;
	}
	
	public function teho($tarkkuus=2){
		return round($tehty/$kesto, $tarkkuus);
	}
	
	public function kesto($uusi=null){
		if(isset($uusi)){$this->kesto = $uusi;}
		return $this->kesto;
	}
	
	public function tehty($uusi=null){
		if(isset($uusi)){$this->tehty = $uusi;}
		return $this->tehty;
	}
	
	public function numero($uusi=null){
		if(isset($uusi)){$this->numero = $uusi;}
		return $this->numero;
	}
	
	public function kapa(){
		return Kapa::laskeKapa($this->numero, $this->tehty, $this->kesto);
	}

}


?>
