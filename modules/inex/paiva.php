<?php


class Paiva{

	public $putket= array();
	public $paivays = null;
	
	public function __construct($paivays, $putket=null){
		$this->paivays = $paivays;
		if(isset($putket)){
			$this->putket = $putket;
		}
		
#		var_dump($paivays);
#		echo "<br/>\n";
#		var_dump($putket);
#		echo "<br/>\n";
#		echo "<br/>\n";
		
	}
	
	public function lisaaPutki($putki){
		$this->putket[$putki->numero()] = $putki;
	}
	
	public function putket($uusi=null){
		if(isset($uusi)){$this->putket = $uusi;}
		return $this->putket;
	}

	public function paivays($uusi=null){
		if(isset($uusi)){$this->paivays = $uusi;}
		return $this->paivays;
	}
	
	public function tehty($numero=null){
		$yhteensa = 0;
		foreach($this->putket as $putki){
			if(isset($numero)){
				if($putki->numero == $numero){
					$yhteensa += $putki->tehty();
				}
			}else{
				$yhteensa += $putki->tehty();
			}
		}
		return $yhteensa;
	}
	
	public function kesto($numero=null){
		$yhteensa = 0;
		foreach($this->putket as $putki){
			if(isset($numero)){
				if($putki->numero == $numero){
					$yhteensa += $putki->kesto();
				}
			}else{
				$yhteensa += $putki->kesto();
			}
		}
		return $yhteensa;
	}
	
	public function tehot(){
		$tehot = array();
		foreach($this->putket as $putki){
			$tehot[$putki->numero()] = $putki->teho();
		}
		
		return $tehot;
	}
	
	public function onkoNumeroa($numero){
		foreach($this->putket as $putki){
			if($putki->numero == $numero){return true;}
		}
		return false;		
	}

}

?>
