<?php

include("kapa.php");
include("palkka.php");
include("putki.php");
include("paiva.php");
include("kuukausi.php");
include("disOrganisaattori.php");
include("muut.php");
include("lisays.php");
include("muokkaa.php");

/**
* THCMS Inex TyöaikalaskuriModuuli
*/
class inexModule{

	private $nakyma,$muokkaus=null;

	public function asetaSivut($nakyma, $muokkaus){
		$this->nakyma = $nakyma;
		$this->muokkaus = $muokkaus;
	}
	
	public function tulostaTNtaulukko(){
		return tulostaTNtaulukko();
	}

	public function handlaa(){
		Lisays::handlaaFormi();
		Muokkaus::handlaaPoisto();
	}
	
	public function tulostaMuokkausForm(){
		$r = "";
		if(isset($_GET['id'])){
			$id = $_GET['id'];
			$r = Muokkaus::tulostaFormi($id, $this->nakyma);
		}
		
		return $r;
	}
	
	public function tulostaLisaaForm(){
		$kk = date("d.m.Y");
		if(isset($_GET['kk'])){
			$kk =$_GET['kk'];
		}
		
		return Lisays::tulostaFormi($kk, $_GET['page']);
	}
	
	public function run(){
		return $this->tulostaTehoTaulukko();
	}
	
	public function tulostaTehoTaulukko(){
		$html = "";
		/**
		* Hae määritelty päivä; jos ei määritelty niin tämä päivä.
		*/
		$kk = date("d.m.Y");
		if(isset($_GET['kk'])){
			$kk =$_GET['kk'];
		}
		
		/**
		* Tulosta Kuunvaihtolinkit
		*/
		$kuukaudet = haeEdellinenJaSeuraavaKuukausi($kk);
		$html.= '<div class="span1"><a href="index.php?page='.$this->nakyma.'" >Nyt</a></div>
		  <div class="span2"><a href="index.php?page='.$this->nakyma.'&kk='. $kuukaudet["edellinen"].'" >Edellinen kuu</a></div>
		  <div class="span2"><a href="index.php?page='.$this->nakyma.'&kk='. $kuukaudet["seuraava"].'" >Seuraava kuu</a></div>
		  <br/><br/>
		';
		
		$kuukausi = lataaArvotTietokannasta($kk);
		$html.= tulostaTehoTaulukko($kuukausi);
		
		
		return $html;
	}
	
	public function disorganisaattori($pv=0){
		$a = new DisOrganisaattori();
		if($pv == 0){
			return $a->laskeNytOrganisaattoriArvaus();
		}else if($pv > 0){
			return $a->laskeEdeltavaOrganisaattoriArvaus($pv);
		}else if($pv < 0){
			return $a->laskeTulevaOrganisaattoriArvaus(0-$pv);
		}
		
	}
	
	public function tulostaPS(){
		$kk = date("d.m.Y");
		if(isset($_GET['kk'])){
			$kk =$_GET['kk'];
		}
		$kuukausi = lataaArvotTietokannasta($kk);
		return tulostaPaivasaldot($kuukausi, $this->muokkaus);
	}
	
	public function teeTNKaavio(){
		$a = new DisOrganisaattori();
		$kestot = $a->kaikki()["kestot"];
		$tn = $a->kaikki()["tn"];
		
		ksort($kestot);
		ksort($tn);
		
		$arvot = array();
		$tnl = array();
		foreach($kestot as $k=>$v){
			$arvot[] = $v;
			$tnl[] = $k . " (" . round(100*$tn[$k],2) .")";
		}
		$arvot = "[" . implode(",", $arvot) . "]";
		$tnl = "['" . implode("','", $tnl) . "']";
		
		$basedir =  BASEDIR;
		$cu = $GLOBALS['current_url'];
		
		return "
	<script src=\"$cu/modules/inex/libraries/RGraph.common.core.js\" ></script>
	<script src=\"$cu/modules/inex/libraries/RGraph.common.dynamic.js\" ></script>
	<script src=\"$cu/modules/inex/libraries/RGraph.common.tooltips.js\" ></script>
	<script src=\"$cu/modules/inex/libraries/RGraph.pie.js\" ></script>
	
	<canvas id=\"cvspie\" width=\"350\" height=\"250\">[No canvas support]</canvas>
	
    <!--
        3/3. This creates and displays the graph. As it is here, you can call this from the window.onload event,
             allowing you to put it in your pages header.
    -->
    <script>
        window.onload = function ()
        {
            var pie = new RGraph.Pie('cvspie', $arvot);
            pie.Set('chart.labels', $tnl);
            pie.Set('chart.shadow', true);
            pie.Set('chart.shadow.offsetx', 0);
            pie.Set('chart.shadow.offsety', 0);
            pie.Set('chart.strokestyle', 'transparent');
            pie.Set('chart.tooltips', $tnl);
            pie.Set('chart.colors', ['black','white','gray','green','blue','red','yellow','aqua','chocolate','greenyellow','magenta','navy','olive','teal']);
            pie.Draw();
       }
    </script>
    ";
	
	
	}
	
	
	public function teeDOKaavio($monta=14, $tulevia=0){
		$current_url = $GLOBALS['current_url'];
		$arvot = array();
		$x = range(0,$monta);
		$pienin = null;
		$suurin = null;
		
		$keskiarvo = 0;
		$maara = 0;
		
		for($i=$monta; $i>=0; $i--){
			$temp = $this->disorganisaattori($i);
			$arvot[] = array($monta-$i, round($temp,0));
			
			$keskiarvo += $temp;
			$maara += 1;
			
			
			if(!isset($pienin) || $pienin > $temp){
				$pienin = $temp;
			}
			if(!isset($suurin) || $suurin < $temp){
				$suurin = $temp;
			}
		}
		
		$b = array();
		foreach($arvot as $a){
			$b[] = "[ " . implode(", ",$a) . " ]";
		}
		
		$arvot = "[ " . implode(", ", $b) . " ]";
		
		$x = "[' " . implode("', '", $x) . "' ]";
		
		# Tulevat
		$tulevat = array();
		
		for($i=0; $i<=$tulevia; $i++){
			$t1 = $this->disorganisaattori(0-$i);
			$tulevat[] = "[$monta, $t1, 'green']";
			$monta += 1;
			
			if(!isset($pienin) || $pienin > $t1){
				$pienin = $t1;
			}
			if(!isset($suurin) || $suurin < $t1){
				$suurin = $t1;
			}
		}
		
		$pienin -= 5;
		$suurin += 5;
		
		$monta -= 1;
		$tulevat = "[ " . implode(", ", $tulevat) . " ]";
		
		$keskiarvo = $keskiarvo / $maara;
		$keskiarvo = "[ [ 0, $keskiarvo, 'orange' ], [ $monta, $keskiarvo, 'orange' ] ]";
		
return"
	<script src=\"$current_url/modules/inex/libraries/RGraph.common.core.js\" ></script>
	<script src=\"$current_url/modules/inex/libraries/RGraph.common.dynamic.js\" ></script>
	<script src=\"$current_url/modules/inex/libraries/RGraph.common.tooltips.js\" ></script>
	<script src=\"$current_url/modules/inex/libraries/RGraph.scatter.js\" ></script>
	
	<canvas id=\"cvs\" width=\"1000\" height=\"250\">[No canvas support]</canvas>
	
    <!--
        3/3. This creates and displays the graph. As it is here, you can call this from the window.onload event,
             allowing you to put it in your pages header.
    -->
<script>
    window.onload = function ()
    {
       \"use strict\";
       var arvot = $arvot;
       var tulevat = $tulevat;

       var keskiarvo = $keskiarvo;
       var scatter = new RGraph.Scatter('cvs', arvot, tulevat, keskiarvo);
       scatter.Set('chart.xmax', $monta);
       
       scatter.Set('chart.ymin', $pienin);
       scatter.Set('chart.ymax', $suurin);
       scatter.Set('chart.line', true);
       scatter.Set('chart.line.linewidth', 1);
       scatter.Set('chart.line.colors', ['red']);
       scatter.Set('chart.line.shadow.color', '#999');
       scatter.Set('chart.line.shadow.blur', 15);
       scatter.Set('chart.line.shadow.offsetx', 0);
       scatter.Set('chart.line.shadow.offsety', 0);
       
       //scatter.Set('chart.labels', $x);
       scatter.Draw();
    };
</script>
	";	
	}
	
}


?>
