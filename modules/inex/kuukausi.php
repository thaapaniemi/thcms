<?php

class Kuukausi{
	
	public $paivat = array();
	protected $aikaleima = null;
	
	public function __construct($aikaleima){
		$this->aikaleima = $aikaleima;		
	}
	
	public function lisaaPaiva($paiva){
		$this->paivat[$paiva->paivays()] = $paiva;
	}
	
	public function paivat($uusi=null){
		if(isset($uusi)){$this->paivat = $uusi;}
		return $this->paivat;
	}
	
	public function tehty($numero = null){
		$yhteensa = 0;
		foreach($this->paivat as $paiva){
			$yhteensa += $paiva->tehty($numero);
		}
		
		return $yhteensa;
	}
	
	
	public function kesto($numero = null){
		$yhteensa = 0;
		foreach($this->paivat as $paiva){
			$yhteensa += $paiva->kesto($numero);
		}
		
		return $yhteensa;
	}
	
	public function tehot(){
		$tehot = array();
		$numerot = range(0, 1000);
		
		foreach($numerot as $numero){
			
			if( $this->tehty($numero) != 0 && $this->kesto($numero) != 0){
				if(!isset($tehot[$numero])){
					$tehot[$numero] = array();
					$tehot[$numero]["kesto"] = 0;
					$tehot[$numero]["tehty"] = 0;
				}
				
				$tehot[$numero]["kesto"] += $this->kesto($numero);
				$tehot[$numero]["tehty"] += $this->tehty($numero);
			}
		}
		
		return $tehot;
	}
	
	public function tyoPaivia(){
		return count($this->paivat);
	}
	
	public function paiviaLoppukuussa(){
		$tamapaiva = $this->aikaleima;
		if( (int)date("m") != (int)date("m",$tamapaiva) ){
			#return 0;
		}
		
		$ekapaiva = date("Ym01", $this->aikaleima);
		$vikapaiva = date("Ymt", $this->aikaleima);
		
		
		
		$yhteensa = laskePaivat($ekapaiva, $vikapaiva , array());
		$tahanmennessa = laskePaivat($ekapaiva, date("Ymd",$tamapaiva) , array());
		$erotus = $yhteensa - $tahanmennessa;
		
		return $erotus;
	}
	
	public function minuuttejaJaljella(){
		return $this->paiviaLoppukuussa()*7.5*60;
	}
	
	public function laskeNumeroPaivat($numero){
		$count = 0;
		foreach($this->paivat as $paiva){
			if($paiva->onkoNumeroa($numero)){
				$count += 1;
			}
		}
		
		return $count;
	}
	
	
}

function laskePaivat($alku, $loppu){

	$alku = strtotime($alku);
	$loppu = strtotime($loppu);
	
	$paivia = ($loppu - $alku) / 84600 +1;
	$nyt = $alku;
	
	$laskuri = 0;
	
	for($nyt=$alku; $nyt<$loppu; $nyt +=84600){
		$n = (int) date("N", $nyt);
		
		if( $n != 6 && $n != 7 ){
			$laskuri += 1;
		}else{}
	}
	
	return $laskuri;
}


function getWorkingDays($startDate,$endDate,$holidays=array() ){
    // do strtotime calculations just once
    $endDate = strtotime($endDate);
    $startDate = strtotime($startDate);


    //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
    //We add one to inlude both dates in the interval.
    $days = ($endDate - $startDate) / 86400 + 1;

    $no_full_weeks = floor($days / 7);
    $no_remaining_days = fmod($days, 7);

    //It will return 1 if it's Monday,.. ,7 for Sunday
    $the_first_day_of_week = date("N", $startDate);
    $the_last_day_of_week = date("N", $endDate);

    //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
    //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
    if ($the_first_day_of_week <= $the_last_day_of_week) {
        if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
        if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
    }
    else {
        // (edit by Tokes to fix an edge case where the start day was a Sunday
        // and the end day was NOT a Saturday)

        // the day of the week for start is later than the day of the week for end
        if ($the_first_day_of_week == 7) {
            // if the start date is a Sunday, then we definitely subtract 1 day
            $no_remaining_days--;

            if ($the_last_day_of_week == 6) {
                // if the end date is a Saturday, then we subtract another day
                $no_remaining_days--;
            }
        }
        else {
            // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
            // so we skip an entire weekend and subtract 2 days
            $no_remaining_days -= 2;
        }
    }

    //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
//---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
   $workingDays = $no_full_weeks * 5;
    if ($no_remaining_days > 0 )
    {
      $workingDays += $no_remaining_days;
    }

    //We subtract the holidays
    foreach($holidays as $holiday){
        $time_stamp=strtotime($holiday);
        //If the holiday doesn't fall in weekend
        if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
            $workingDays--;
    }

    return $workingDays;
}

?>
