<?php

class Palkka{

	static private $tuntipalkka = 10.69;
	
	// Iltalisä 18-> 3.65 e/h
	// keskiarvoistettuna (1/4)*lisä
	// Vastuulisä 5%
	
	static private $lisat = 1.447;
	
	public function __construct(){}
	
	static public function laskePerusPalkka($minuutit){
		$tunnit = $minuutit / 60.0;
		return $tunnit * Palkka::$tuntipalkka;
	}
	
	static public function laskeLisat($minuutit){
		$tunnit = $minuutit / 60.0;
		return $tunnit * Palkka::$lisat;
	}
	
	static public function laskeKokoPalkka($minuutit){
		$palkka = Palkka::laskePerusPalkka($minuutit) + Palkka::laskeLisat($minuutit);
		
		return $palkka;
	}

}



?>
