<?php


class Kapa{

	public static $taulukko = array(
		0 => 0,
		801=> -1,
		802=> -1,
		805=>1.15,
		807=>1.15,
		808=>1.12,
		811=>1.15,
		816=>1.12,
		814=>-1,
		815=>-1,
		816=>1.15,
		819=>1.20,
		820=>1.05,
		822=>1.12,
		824=>1.10,
		825=>1.15,
		845=>1.15,
		850=>0,
		855=>-1, // oikeasti keskiteho
		890=>1,
		998=>0, // Lapsen sairaus
		999=>0, // korjaa sairauspoissaoloksi
	);

	// korjaa näyttämään oikea kerroin
	public static function tehokkuusTaulukko($teho){
		// 95 => 0
		$korjattuteho = ($teho * 100) - 95;
		if($korjattuteho <= 0){
			return 0;
		}
		return 0.45 + ($korjattuteho * 0.0526);	
	}

	public static function laskeKapa($numero, $tehty, $kesto){
		if($numero == 850){
			return 0;
		}
		
		if(Kapa::laskeKannuste($numero, $tehty, $kesto) == 0){
			return 0;
		}
		
		return round (Kapa::laskeKannuste($numero, $tehty, $kesto) + Kapa::laskeVastuuLisa($kesto) + Kapa::laskeLaatuLisa($kesto) + Kapa::laskeSiivousLisa($kesto), 2);
	}

	public static function laskeKannuste($numero, $tehty, $kesto){
		$tehokerroin = 1;
		if (isset(Kapa::$taulukko[$numero])){
			$tehokerroin = (double) Kapa::$taulukko[$numero];
		}
		
		if($tehokerroin == -1){
			if($tehty != 0 && $kesto != 0){
				$tehokerroin = (double) $tehty / $kesto;
			}
		}
		$rkerroin = Kapa::tehokkuusTaulukko($tehokerroin);
		
		return $rkerroin * ($kesto / 60.0);
	}

	public static function laskeVastuuLisa($aika){
		return 0.20 * ($aika / 60.0);
		return 0;
	}
	
	// 100% laatu
	public static function laskeLaatuLisa($aika){
		return 0.32 * ($aika / 60.0);
	}

	// 100% siivous
	public static function laskeSiivousLisa($aika){
		//TODO: Laske keskiarvo
		return 0.24 * ($aika / 60.0);
	}
	
	
	public static function laskeKeskiarvoKapa($minuutit){
		//TODO:##
	}

}



?>
