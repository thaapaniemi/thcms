<?php

class DisOrganisaattori{

	public $tn = array();
	public $tehot = array();
    public $kestot = array();
    
    public $kerroin = array(

            801 => 0,
            802 => 0,
            814 => 0.25,
            815 => 0.25,
            850 => 0,
            890 => 0,
            998 => 0.5,
            999 => 0.5,
        );
	
	public function __construct(){
		$miinus4kk = strtotime( "-4 Months");
		
		$arvot = ORM::for_table("inex")->where_gte('timestamp', $miinus4kk)->find_many();
		
		$kestotaulu = array();
		$kestoyht = 0;
		
		foreach($arvot as $arvo){
			if( !isset($kestotaulu[$arvo->numero]) ){
				$kestotaulu[$arvo->numero] = array();
				$kestotaulu[$arvo->numero]["kesto"] = 0;
				$kestotaulu[$arvo->numero]["tehty"] = 0;
			}
			
			$kestotaulu[$arvo->numero]["kesto"] += $arvo->kesto;
			$kestotaulu[$arvo->numero]["tehty"] += $arvo->tehty;
			$kestoyht += $arvo->kesto;
		}
		
		foreach($kestotaulu as $numero => $arvot){
        	$this->kestot[$numero] = $arvot["kesto"];
        }
		
		foreach($kestotaulu as $numero => $t){
			if($t["kesto"] != 0){
				$this->tehot[$numero] = $t["tehty"] / $t["kesto"];
			}else{
				$this->tehot[$numero] = 0;
			}
				$this->tn[$numero] =  $t["kesto"] / $kestoyht;
		}
	}
	
	public function tn($numero){
		if(!isset( $this->tn[$numero] )){return 0;}
		else{return $this->tn[$numero];}
	}

    public  function kesto($numero){
        if(!isset( $this->kesto[$numero] )){return 0;}
        else{return $this->kesto[$numero];}
    }
	
	public function kaikki(){
		return array( "tehot" => $this->tehot, "tn"=>$this->tn, "kestot"=>$this->kestot  );
	}
	
	public function laskeTulevaOrganisaattoriArvaus($montapv){
		$montapv = ($montapv) * (60*60*24);

        $huominen = date("Y-m-d", strtotime( "Tomorrow"));
        $e = explode("-", $huominen);
        $huominen = strtotime($e[0]."-".$e[1]."-".$e[2]);

        $v1 = strtotime( "-4 Months") + $montapv;
        $v2 = $huominen;
        $arvot = ORM::for_table("inex")->where_gte('timestamp', $v1)->where_lte('timestamp', $v2)->find_many();

        $kerroin = $kerroin = $this->kerroin;

        $yht = 0;
        foreach($arvot as $arvo){
            $k = 1;

            if(isset($kerroin[$arvo->numero])){
                $k = $kerroin[$arvo->numero];
            }

            $yht += $k * $arvo->kesto;
        }


        return round(($yht / 60.0),2);
	}

    public function laskeNytOrganisaattoriArvaus(){

        $kerroin = $this->kerroin;

        $yht = 0;
        foreach($this->kestot as $numero => $arvo){
            $k = 1;

            if(isset($kerroin[$numero])){
                $k = $kerroin[$numero];
            }

            $yht += $k * $arvo;
        }


        return round(($yht / 60.0),2);
    }

    public function laskeEdeltavaOrganisaattoriArvaus($montapv){
        $montapv = ($montapv -1) * (60*60*24);

        $eilinen = date("Y-m-d", strtotime( "Yesterday"));
        $e = explode("-", $eilinen);
        $eilinen = strtotime($e[0]."-".$e[1]."-".$e[2]);

        $v1 = strtotime( "-4 Months") - $montapv;
        $v2 = $eilinen - $montapv;
        $arvot = ORM::for_table("inex")->where_gte('timestamp', $v1)->where_lte('timestamp', $v2)->find_many();

        $kerroin = $kerroin = $this->kerroin;

        $yht = 0;
        foreach($arvot as $arvo){
            $k = 1;

            if(isset($kerroin[$arvo->numero])){
                $k = $kerroin[$arvo->numero];
            }

            $yht += $k * $arvo->kesto;
        }


        return round(($yht / 60.0),2);

    }

}


?>
