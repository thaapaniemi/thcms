<?php

require_once("kapa.php");
require_once("palkka.php");
require_once("putki.php");
require_once("paiva.php");
require_once("kuukausi.php");
require_once("disOrganisaattori.php");
require_once("muut.php");

class Muokkaus{

	static public function handlaaPoisto(){
		if(isset($_GET['poista']) && isset($_GET['id']) && $_GET['poista'] == "true"){
			Muokkaus::poista($_GET['id']);
		}
	}

	static public function tulostaFormi( $id, $nakyma){
		$putki = ORM::for_table("inex")->where('id', $id)->find_one();
		
		if(!$putki){
			return null;
		}
		
		$pvm = $putki->p.".".$putki->k.".".$putki->v;
		$numero = $putki->numero;
		$tehty = $putki->tehty;
		$kesto = $putki->kesto;
		$uuid = $putki->id;
		
		$form = "
<form id='muokkaus' type='index.php' method='GET'>
		pvm: 		<input id='muokkaus' type='text' name='pvm' placeholder='pvm' value='". $pvm ."' /><br/>
		työnumero:	<input id='muokkaus' type='text' name='numero' placeholder='työnumero' value='". $numero ."'/><br/>
		tehty:		<input id='muokkaus' type='text' name='tehty' placeholder='tehty' value='". $tehty ."' /><br/>
		kesto:		</td><td><input id='muokkaus' type='text' name='kesto' placeholder='kesto' value='". $kesto ."' /><br/>
		<input id='muokkaus' type='hidden' name='handle' value='yes' />
		<input id='muokkaus' type='hidden' name='uuid' value='". $uuid ."' />
		<input id='muokkaus' type='hidden' name='page' value='".$nakyma."' />
		<input id='muokkaus' type='submit' name='submit' value='Lähetä' />
</form>	

<br/><br/><br/><br/>\n\n
	<a href='index.php?page=$nakyma&handle=true&poista=true&id=$id'>Poista $pvm:$numero ($id)</a><br/>\n
		";
		
		return $form;
	}
	
	static public function poista($id){
		$putki = ORM::for_table("inex")->where('id', $id)->find_one();
		if(!$putki){
			return false;
		}
		
		$putki->delete();
		return true;
	}

}


?>
