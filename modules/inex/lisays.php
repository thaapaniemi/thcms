<?php

#include_once("lib.uuid.php");

class Lisays{

	static public function tulostaFormi( $pvm, $page  ){
		#=date('d.m.Y')
		$pvma = Lisays::muutaPaiva($pvm);
	
		$putket = ORM::for_table("inex")->where('p', $pvma['p'])->where('k', $pvma['k'])->where('v', $pvma['v'])->find_many();
		
		$yhteensa = 450;
		foreach($putket as $putki){
			$yhteensa -= $putki->kesto;
		}
		if($yhteensa < 0){$yhteensa = 0;}
		
		
		$form = "
<form id='lisays' type='index.php' method='GET'>
		pvm: 		<input id='lisays' type='text' name='pvm' placeholder='pvm' value='". $pvm ."' /><br/>
		työnumero:	<input id='lisays' type='text' name='numero' placeholder='työnumero' /><br/>
		tehty:		<input id='lisays' type='text' name='tehty' placeholder='tehty' /><br/>
		kesto:		</td><td><input id='lisays' type='text' name='kesto' placeholder='kesto' value='". $yhteensa ."' /><br/>
		<input id='lisays' type='hidden' name='handle' value='yes' />
		<input id='lisays' type='hidden' name='page' value=$page />
		<input id='lisays' type='hidden' name='uuid' value='". UUID::mint( 1 ) ."' />
		<input id='lisays' type='submit' name='submit' value='Lähetä' />
</form>		
		";
		
		return $form;
	}
	
	/** 
	* Form kellonajalliseen lisäykseen (laskee minuutit ajasta)
	*
	*/
	static public function tulostaFormi2( $pvm, $page  ){
		#=date('d.m.Y')
		$pvma = Lisays::muutaPaiva($pvm);
				
		$form = "
<form id='lisays2' type='index.php' method='GET'>
		pvm: 		<input id='lisays2' type='text' name='pvm' placeholder='pvm' value='". $pvm ."' /><br/>
		työnumero:	<input id='lisays2' type='text' name='numero' placeholder='työnumero' /><br/>
		aloitus: <input id='lisays2' type='text' name='aloitusaika' placeholder='aloitusaika' /><br/>
		lopetus: <input id='lisays2' type='text' name='lopetusaika' placeholder='lopetusaika' /><br/>
		tehty: <input id='lisays2' type='text' name='tehty' placeholder='tehty' /><br/>
		<input id='lisays2' type='hidden' name='handle' value='yes' />
		<input id='lisays2' type='hidden' name='page' value=$page />
		<input id='lisays2' type='hidden' name='uuid' value='". UUID::mint( 1 ) ."' />
		<input id='lisays2' type='submit' name='submit' value='Lähetä' />
</form>		
		";
		
		return $form;
	}
	
	static public function handlaaFormi(){
		
		if(isset($_GET['pvm']) && isset($_GET['kesto']) && isset($_GET['numero']) && isset($_GET['uuid'] )){
			$pvm = $_GET['pvm'];
			$kesto = $_GET['kesto'];
			$numero = $_GET['numero'];
			$uuid = $_GET['uuid'];
			
			$ok = false;
			$pvme = Lisays::muutaPaiva($pvm);
			if($pvme){$ok=true;}
			
			if(strpos($kesto, "h")){
				$kesto = str_replace("h","",$kesto);
				$kesto = 60 * $kesto;
			}
			
			$tehty = $kesto;
			if(isset($_GET['tehty'])){ 
				$tehty = $_GET['tehty'];
				if(strpos($tehty, "h")){
					$tehty = str_replace("h","",$tehty);
					$tehty = 60 * $tehty;
				}
			}
			
			// Muuta tn. lavat minuuteiksi
			if($tehty < 0.5*$kesto){
				$tehty = Lisays::korjaaTehot($numero, $tehty);
			}
			
			$putki = ORM::for_table("inex")->where('id', $uuid )->find_one();
			
			if($ok){
				
				if(!$putki){
					$putki = ORM::for_table('inex')->create();
				}
				
				$putki->id = $uuid;
				$putki->p = $pvme['p'];
				$putki->k = $pvme['k'];
				$putki->v = $pvme['v'];
				$putki->timestamp = strtotime($pvme['p'] . "." . $pvme['k'] . ".". $pvme['v']);
				
				$putki->numero = $numero;
				$putki->kesto = $kesto;
				$putki->tehty = $tehty;
				
				$putki->save();
				return true;
			}
			else{}
		}
		
		
		return false;
	}
	
	static public function handlaaFormi2(){
		
		# tähän tarkistus että tarvittavat tiedot löytyvät
		$aloitus = $_REQUEST['aloitusaika'];
		$lopetus = $_REQUEST['lopetusaika'];
		
		$aloitus = explode($aloitus, ":");
		$lopetus = explode($lopetus, ":");
		
		$aloitus = 60*$aloitus[0]+$aloitus[1];
		$lopetus = 60*$lopetus[0]+$lopetus[1];
		$erotuslisa = 0;
		
		if($aloitus >= (10*60) && $aloitus <= (10*60+30)){
			$aloitus = 10*60+30;
		}else if($aloitus >= (18*60) && $aloitus <= (18*60+30)){
			$aloitus = 18*60+30;
		}
		
		if($lopetus >= (10*60) && $lopetus <= (10*60+30)){
			$lopetus = 10*60;
		}else if($lopetus >= (18*60) && $lopetus <= (18*60+30)){
			$lopetus = 18*60;
		}
		
		if($aloitus <= (10*60) && $lopetus >=(10*60+30)){
			$erotuslisa = -30;
		}else if($aloitus <= (18*60) && $lopetus >=(18*60+30)){
			$erotuslisa = -30;
		}
		
		$erotus = ($lopetus - $aloitus) + $erotuslisa;
		if($erotus < 0){
			$erotus = 0;
		}
		
		#tarkista onko olemassa jo numerolla päivälle merkintä ja lisää minuutit siihen
	}
	
	
	public static function korjaaTehot($numero, $lavat){
	
		$korjaustaulukko = array(
			814 => 4.372, // Alaslasku, 4.372 min/lava [keskiarvo 11-12/2012]
			815 => 3.9475 // Hyllytys, 3.9475 min/lava [keskiarvo 11-12/2012]
		);
		
		if(isset($korjaustaulukko[$numero])){
			return $korjaustaulukko[$numero] * $lavat;
		}else{
			return $lavat;
		}
	
	}
	
	protected static function muutaPaiva($pvm){
		$ok =false;
		$pvme = explode(".", $pvm);
			if(sizeof($pvme) == 3){
					if($pvme[2] >= 2012 && $pvme[2] <= 2015){
						if($pvme[1] >= 1 && $pvme[1] <= 12){
							if($pvme[0] >= 1 && $pvme[0] <= (int)date("t", strtotime($pvm))){
								$ok = true;
							}
						}
					}
			}
			
			if($ok){
				return array("p"=>$pvme[0], "k"=>$pvme[1], "v"=>$pvme[2]);
			}else{
				return false;
			}
	}


	
}

#echo Lisays::tulostaFormi($pvm); 

?>

