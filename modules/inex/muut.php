<?php

function lataaArvotTietokannasta($kk){
	$kkk = explode(".",$kk);
	$paivat = array();
	$putket = ORM::for_table("inex")->where('k', $kkk[1] )->where('v', $kkk[2] )->find_many();
	foreach($putket as $putki){
		$p = new Putki($putki->numero, $putki->tehty, $putki->kesto, $putki->id);
		
		if(!isset($paivat[$putki->p])){$paivat[$putki->p] = new Paiva($putki->p);}
		$paivat[$putki->p]->lisaaPutki($p);
	}

	$kuukausi = new Kuukausi(strtotime($kk));
	foreach($paivat as $paiva){
		$kuukausi->lisaaPaiva($paiva);
	}
	
	return $kuukausi;
}

function haeEdellinenJaSeuraavaKuukausi($kk){
	$edellinen = explode(".",$kk);
	$edellinen[0] = 0;
	$edellinen = implode(".", $edellinen);
	$edellinen = strtotime($edellinen);
	$edellinen -= 24*60*60+1; // Vähennetään yli päivän verran sekunteja ensimmäisestä päivästä
	if($edellinen < strtotime("today")){
		$edellinen = date("t.m.Y", $edellinen);
	}else if($edellinen > strtotime("today")){
		$edellinen = date("01.m.Y", $edellinen);
	}
	
	$seuraava = strtotime( date("t.m.Y", strtotime($kk)));
	$seuraava += 24*60*60+1; // Lisätään yli päivän verran sekunteja viimeiseen päivään
	if($seuraava < strtotime("today")){
		$seuraava = date("t.m.Y", $seuraava);	
	}else if($seuraava > strtotime("today")){
		$seuraava = date("01.m.Y", $seuraava);	
		}

	return array("edellinen" => $edellinen, "seuraava" => $seuraava);
}

function laskeLoppukuunKapa($kuukausi){
	// Kapan laskenta loppukuulle
	$paivia = $kuukausi->paiviaLoppukuussa();
	$minuutteja = $paivia * 7.5 * 60;
	$tn0 = new DisOrganisaattori();
	$tntaulu = $tn0->kaikki();
	
	$todennakoisyydet = $tntaulu["tn"];
	$tehot = $tntaulu["tehot"];
	
	$loppukuunkapa = 0;
	foreach($todennakoisyydet as $numero => $tn){
		$tehty = ($tehot[$numero]*$minuutteja * $tn);
		$kesto = ($tn*$minuutteja);
		$loppukuunkapa += Kapa::laskeKapa($numero, $tehty, $kesto);
	}
	
	return $loppukuunkapa;
}

function tulostaPaivasaldot($kuukausi, $muokkaus = ""){
	$html = "";
	
	$taulu = array();
	$taulu[] = array("päivä","minuuttia","numerot") ;
	$html.= "<br/>\n";
	$html.= "Päiväsaldot:<br/>\n";
	$html.= "<table border='0'>\n";
	$html.= "<tr>
	<td align='center'>Päivä</td>
	<td align='center'>Minuuttia</td>
	<td align='center'>Numerot</td>
	</tr>
	";
	
	$counter = 0;
	$paivat = $kuukausi->paivat();
	ksort($paivat);
	foreach($paivat as $k => $v){
		$temp = array();
	
		if($counter % 2 == 0){
			$html.= "<tr bgcolor='#EEEEEE'>\n";
		}else{
			$html.= "<tr bgcolor='#FFFFFF'>\n";
		}	
	
		$html.= "<td>";
		$html.= "$k";
		$html.= "</td>";
		$temp[] = $k;
		
		$html.= "<td>";
		$html.= $v->kesto();
		$html.= " min";
		$html.= "</td>";
		$temp[] = $v->kesto();
		
		$html.= "<td>";
		$n = array();
		foreach($v->putket() as $numero => $putki){
			$nn = "<a href='index.php?page=$muokkaus&id=" . $putki->id . "'>$numero</a>";
			$n[] = $nn;
			#$n[] = $numero;
		}
		
		sort($n);
		$html.= implode(",", $n);
		$temp[] = implode(",", $n);
		
		$html.= "</td>";
		$html.= "</tr>\n";
		
		#$html.= "}<br/>\n";
		$counter +=1;
		
		$taulu[] = $temp;
	}
	
	$html.= "</table>\n";
	
	$html = teeTaulu($taulu);
	return $html;
	
}

function laskeKapa($kuukausi){
	$kapa = 0;
	foreach($kuukausi->tehot() as $numero => $arvot){
		$kapa += Kapa::laskeKapa($numero, $arvot["tehty"], $arvot["kesto"]);
	}
	
	return $kapa;
}

function tulostaTehoTaulukko($kuukausi){
	#$tt = new Taulu(array( "table"=>array("border"=>"0"), "td"=>array("align"=>"center") ));
	$taulu = array();
	$taulu[] = array("numero", "balanssi", "tehty/kesto", "teho", "osuus", "päiväosuus", "kapa");
	#$tt->lisaaRivi(array("numero", "balanssi", "tehty/kesto", "teho", "osuus", "päiväosuus", "kapa"));
	
	$yhteensa_aika = 0;
	$yhteensa_raha = 0;
	foreach($kuukausi->tehot() as $numero => $arvot){
		$k = Kapa::laskeKapa($numero, $arvot["tehty"], $arvot["kesto"]);
		$teho = round(100* ($arvot["tehty"] / $arvot["kesto"]), 2);
		$erotus = $arvot["tehty"] - $arvot["kesto"];
		$paivataso = round(100* $kuukausi->laskeNumeroPaivat($numero) / $kuukausi->tyoPaivia(), 1);
		
		$vari = "black";
		if($teho < 100){ $vari = "red";}
		else if($teho > 100){ $vari = "green";}
		
		$temp = array();
		#Numero
		$temp[] .= "<font color='" . $vari . "'>". $numero ."</font>";
	
		#Balanssi (tehty-kesto)
		$lavat = "";
		if($numero == 814){
			$lavat = " [" . round(($erotus / 4.372),0) . " L]";
			
		}else if($numero == 815){
			$lavat = " [" . round(($erotus / 3.9475),0) . " L]";
		}
		$temp[] .= "<font color='" . $vari . "'>". $erotus . $lavat ."</font>";
		
		#Tehty/kesto
		$temp[] .= "<font color='" . $vari . "'>". $arvot["tehty"] . " / " . $arvot["kesto"] ." min</font>";
		$yhteensa_aika += $arvot["kesto"];
		
		#Teho
		$temp[] .= "<font color='" . $vari . "'>". $teho ." %</font>";
		
		#Osuus
		$temp[] .= "<font color='" . $vari . "'>". round($arvot["kesto"]/$kuukausi->kesto(),3)*100 ." %" ."</font>";
	
		#Päiväosuus
		$temp[] .= "<font color='" . $vari . "'>". $paivataso ." %</font>";
		
		#Kapa
		$temp[] .= "<font color='" . $vari . "'>". $k ." €</font>";
		$yhteensa_raha += $k;

		$taulu[] = $temp;
	}
	
	$taulu[] = array("", "Yhteensä: ",  round($yhteensa_aika/60.0, 1). " h", "", "", "", "" );
	$taulu[] = array("", "Palkka:",  round(Palkka::laskeKokoPalkka($yhteensa_aika),2). " €", "", "", "Kapa:", $yhteensa_raha . " €" );
	
	$yhteensa = Palkka::laskeKokoPalkka($yhteensa_aika)+$yhteensa_raha;
	$palkkaaPuuttuu = Palkka::laskeKokoPalkka( $kuukausi->paiviaLoppukuussa()*7.5*60 );
	$loppukuunkapa = laskeLoppukuunKapa($kuukausi);
	
	$ennuste0 = (1* $yhteensa + $palkkaaPuuttuu + $loppukuunkapa);
	$ennuste = 1275 + 0.1242*$ennuste0 + 0.008421*pow(($yhteensa_raha+$loppukuunkapa),2) - 2.036e4*pow((0.18),2);
	
	
	$taulu[] = array("", "Arvio:",  round($ennuste  ,2). " €", "", "", "", "" );
	
	
	#echo $tt;
	
	return teeTaulu($taulu);
}

function tulostaTNtaulukko(){
	$ignorelist = array(0);
	$tn0 = new DisOrganisaattori();
	$tntaulu = $tn0->kaikki();
	$todennakoisyydet = $tntaulu["tn"];
    $kestot = $tntaulu["kestot"];
	
	$taulu = array();
	$taulu[] = array("numero", " TN ", " tunnit");
	
	foreach($ignorelist as $i){
		if(isset($todennakoisyydet[$i])){
			unset($todennakoisyydet[$i]);
		}
	}
	ksort($todennakoisyydet);
	
	foreach($todennakoisyydet as $numero => $tn){
		$taulu[] = array($numero, round(100*$tn,2) . " %", round($kestot[$numero] /60.0,2) . " h");
	}
	
	return teeTaulu($taulu);
}

function teeTaulu($sisalto){
	$taulu = "<table border='0'>";
	foreach($sisalto as $key=>$value){
		
		if($key % 2 == 0){
			$taulu .="<tr bgcolor='#EEEEEE'>\n";
		}else{
			$taulu .="<tr bgcolor='#FFFFFF'>\n";
		}
		
		foreach($value as $v){
			$taulu .= "<td align='center'>";
			$taulu .= $v;
			$taulu .= "</td>\n";
		}
		$taulu .= "</tr>";
	}
	$taulu .= "</table>";
	return $taulu;
}

?>
