<?php

/**
* TCHMS Files module
* @package THCMS
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/


require_once(BASEDIR ."/core/KV.php");
require_once(BASEDIR ."/3rdparty/lib.uuid.php");

/**
* Files Library class for organizing files
* @module THCMS/modules/Files
*/
class filesLibrary{
	/**
	* array of files
	*/
	private static $filelist;
	
	/**
	* Identification of library
	*/
	private static $uuid;
	
	
	/**
	* Constructor
	* @param string $uuid UUID of library in KV-table
	*/
	public function __construct($uuid){
		if(!isset(filesLibrary::$filelist) || filesLibrary::$uuid != $uuid){
			$temp = (array) KV::get($uuid);
			filesLibrary::$uuid = $uuid;
			
			if(!$temp){
				filesLibrary::$filelist = array();
			}else{
				if($temp['valueType'] == 'filesLibrary'){
					filesLibrary::$filelist = (array) $temp['content'];
				}
			}
			
		}
	}
	
	/**
	* Destructor for saving library to KV-table
	*/
	public function __destruct(){
		$temp = array();
		$temp['valueType'] = 'filesLibrary';
		$temp['content'] = filesLibrary::$filelist;
		
		KV::set(filesLibrary::$uuid, $temp);
	}
	
	/**
	* Add file identification to library
	* @param string $uuid File identification
	*/
	public function add($uuid){
		filesLibrary::$filelist[] = $uuid;
	}
	
	/**
	* Remove identification from library
	* @param string $uuid File identification
	*/
	public function delete($uuid){
		ksort(filesLibrary::$filelist);
		$loc = array_search($uuid, filesLibrary::$filelist);
		if($loc !== false){
			unset(filesLibrary::$filelist[$loc]);
			KV::delete($uuid);
		}
	}
	
	/**
	* Get list of all files
	* @return array of files
	*/
	public function getAll(){
		return filesLibrary::$filelist;
	}
	
}


/**
* FilesModule class definition
* @module THCMS/modules/Files
*/
class filesModule{
	
	/**
	* Library object
	*/
	private $library = null;
	
	/**
	* Constructor
	* @param string $uuid LibraryUUID
	*/
	public function __construct($uuid='9626e373-510d-4edf-93e3-a8127c100a15'){
		$this->library = new filesLibrary($uuid);
	}

	/**
	* Run method for moduleSupport
	* @return string HTML of output
	*/
	public function run(){
		$page = $_GET['page'];
		$html = "";
		if(isset($_FILES["newfile"])){
			if ($_FILES["newfile"]["error"] > 0){}
			else{
				$file = array();
				$file['valueType'] = 'file';
				$file['contentType'] = $_FILES["newfile"]["type"];
				$file['contentLength'] = $_FILES["newfile"]["size"];
				$file['content'] = base64_encode(file_get_contents($_FILES["newfile"]["tmp_name"]));
				#$file['content'] = file_get_contents($_FILES["newfile"]["tmp_name"]);
				$key = (string)UUID::mint(4);
		
				KV::set($key, $file);
				$this->library->add($key);
		
				$html .=  "<a href='". $GLOBALS['current_url'] . "kv_load.php?key=$key'>$key</a>";
			}
		}
		
		$html .= "<form action='index.php?page=$page' enctype='multipart/form-data' method='POST' >
				<input  type='file' name='newfile' /><br/>
				<button type='submit' name='login_request' class='btn'>Send</button>\n</form>";
				
		return $html;
	}
	
	public function handle(){
		if(isset($_GET['files_action']) && isset($_GET['id'])){
			if($_GET['files_action'] == 'delete'){
				$this->library->delete($_GET['id']);
			}
		}
	}
	
	/**
	* List all files
	* @return string output HTML of all files
	*/
	public function listAll(){
		$uuids = (array)$this->library->getAll();
		$page = $GLOBALS['page'];
		
		$html = "";
		foreach($uuids as $u){
			$html .= "<a href='". $GLOBALS['current_url'] . "kv_load.php?key=$u'>$u</a>";
			$html .= "<a href='". $GLOBALS['current_url'] . "index.php?page=$page&id=$u&files_action=delete'> <i class='icon-remove'></i></a>";
			$html .= "<br/>";
		}
		
		return $html;
	}

}
