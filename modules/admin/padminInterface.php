<?php

/**
* AdminTools PageAdminInterface class file
* @package THCMS\AdminTools
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

/**
* Interface class for pageAdmin
* @package THCMS\AdminTools
*/
class PageAdminInterface{
	
	/**
	* TemplateAdminInterface object
	*/
	private $templateAdminInterface = null;

	/**
	* consturctor, arguments pageAdminController and pageDB
	* @param templateAdminInterface $tai templateAdminInterface object
	*/
	function __construct($tai) {
		$this->templateAdminInterface = $tai;
	}
	
	/**
	* destructor
	*/
	function __destruct() {
		$this->pageAdminController = null;
		$this->templateAdminInterface = null;
	}
	
	/**
	* makes and prints a table with links (view and delete) of all pages
	* @param string $basedir url directory prefix for link creation
	*/
	function listAllPages($basedir=""){
		$pages = TPage::getAllPages();
		
		$html = "";
		$html .= "<table>\n";
		foreach($pages as $page){
			$html .= "<tr>\n";
			$html .= $this->generatePageLinks($page,$basedir) . "\n";
			$html .= "</tr>\n";
		}
		$html .= "</table>\n\n";
		
		return $this->templateAdminInterface->listAllPages($html);
	}
	
	/**
	* prints confirmation for page delete
	* @param string $page name of page wanted to be deleted
	* @param string $basedir url directory prefix for link creation
	*/
	function deleteConfirmation($page, $basedir=""){
		$current_page = $_GET['page'];
		
		// You cannot remove Main and Site.menubar
		if($page == MAINPAGE || $page == NAVBAR){
			return;
		}
	
		$html = "";
		$html .= "<p>Do you really want to remove page $page?</p>";
		$html .= "<a href='" . $basedir  ."index.php?page=$current_page'>No.</a><br/><br/>";
		$html .= "<a href='". $basedir ."index.php?page=$current_page&removepage=$page&action=remove'>YES!</a><br/><br/>";
		
		return $this->templateAdminInterface->deleteConfirmation($html);
	}
	
	
	/**
	* prints links for page viewing and deleting
	* TODO: Change to return string of links, not printing them
	* @param string $page name of page which links will be generated
	* @param string $basedir url directory prefix for link creation
	*/
	private function generatePageLinks($page, $basedir){
		$current_page = $_GET['page'];
		$html = "";
		$html .= "<td>";
		$html .= "<a href='" . $basedir . "index.php?page=$page' >$page</a>\n";
		$html .= "</td><td>";
		$html .= "<a href='" . $basedir . "index.php?page=$current_page&removepage=$page&action=dconfirm' > [Remove] </a><br/>\n";
		$html .= "</td>";
		
		return $this->templateAdminInterface->generatePageLinks($html);
	}
	
}	
?>
