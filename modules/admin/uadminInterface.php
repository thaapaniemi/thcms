<?php

/**
* AdminTools UserAdminInterface class file
* @package THCMS\AdminTools
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

/**
* Interface class for userAdmin
* @package THCMS\AdminTools
*/
class UserAdminInterface{
	
	/**
	* TemplateAdminInterface object
	*/
	private $templateAdminInterface = null;
	
	/**
	* constructor, arguments userAdminController and userDB
	* @param templateAdminInterface $tai templateAdminInterface object
	*/
	function __construct($tai) {
		$this->templateAdminInterface = $tai;
	}
	
	/**
	* destructor
	*/
	function __destruct() {
		$this->userAdminController = null;
		$this->templateAdminInterface = null;
	}
	
	/**
	* Makes list of all groups what belongs to user
	* @param $id string id(name) of user
	* @return string HTML removal links of groups
	*/
	public function listAllGroupsOfUser($id){
			$current_page = $_GET['page'];
			$user = new TUser($id, false);
			$groups = $user->getGroups();
			$groups2 = array();
			
			foreach($groups as $g){
				$gclass = "groupread";
				if($g->rw() == "w"){
					$gclass = "groupwrite";
				}
				$groups2[] = "<a class='$gclass' href='index.php?page=$current_page&username=$id&action=ungroup&group=$g'>$g</a>";
			}
			
			$groups2[] = " <a href='index.php?page=$current_page&username=$id&action=addgroup'>Add group</a>";
			
			$html = implode(",", $groups2);
			
			return $html;
	}
	
	/**
	* Prints list with links (change pw, delete) of all users
	*/
	function listAllUsers(){
		$users = TUser::getAllUsers();
		
		$html = "";
		$html .= "<table>\n";
		foreach($users as $user){
			$user = new TUser($user);
			$html .= "<tr>\n<td>";
			$html .= $this->generatePWChangeLink($user);
			$html .= "</td><td> pwid: " . $user->pwhash();
			$html .= "</td>\n";
			
			$html .= "<td>{". $this->listAllGroupsOfUser($user) ."}</td>\n";
			
			$html .= "</tr>\n";
		}
		$html .= "</table>\n";
		
		return $this->templateAdminInterface->listAllUsers($html);
	}
	
	/**
	* print password changing form
	* @param string $basedir url directory prefix for link creation
	*/
	function changePasswordForm($basedir=""){
		$current_page = $_GET['page'];
		$username = $_GET['un'];
		$user = new TUser($username);
		//<input name="username" class="span2" type="text" value="' . $username . '" />
		$html = "";
		$html .= 
			'<form action="' . $basedir .'index.php?page='.$current_page.'&action=pwchange2" method="post"> 
				<input type="hidden" name="oldpwid" value="' .$user->pwhash() .'"/>
				<input type="hidden" name="username" value="' . $username . '" />
				'.$username.':
				<input name="pw" type="password" placeholder="New password" />
				<input type="submit" value="Change Password" />				
			</form>';
		
		return $this->templateAdminInterface->changePasswordForm($html);
	}
	
	/**
	* print user creation form
	* @param string $basedir url directory prefix for link creation
	*/
	function showUserCreationForm($basedir=""){
		$current_page = $_GET['page'];
		$html = "";
		$html .= 
			'<form action="' . $basedir .'index.php?page='.$current_page.'&action=createuser2" method="post"> 
				<input name="username" type="text" placeholder="username" />
				<input name="pw" type="password" placeholder="password" />
				<input type="submit" value="Create User" />				
			</form>';
			
		return $this->templateAdminInterface->showUserCreationForm($html);
	}
	
	/**
	* Show group adding form
	* @param string $username Username who we want to add group
	* @param string $basedir url directory prefix for link creation
	*/
	function showGroupAddingForm($username, $basedir=""){
		$current_page = $_GET['page'];
		$html = "";
		$html .= 
			'<form action="' . $basedir .'index.php?page='.$current_page.'&action=addgroup2" method="post"> 
				<input name="username" type="text" placeholder="username" value="'.$username.'"/>
				<input name="group" type="text" placeholder="group" /><br/>
				<input type="radio" name="rw" value="r">Read<br/>
				<input type="radio" name="rw" value="w">Read/Write
				<br/><input type="submit" value="Add group" />	
			</form>';
			
		return $html;
	}
	
	
	/**
	* print user deletion confirmation
	* @param string $basedir url directory prefix for link creation
	*/
	function deleteUserConfirmation($basedir=""){
		$current_page = $_GET['page'];
		$username = $_GET['un'];
		
		$html = "";
		
		$html .= "Do you want to delete User $username?";
		$html .= "<br/>\n";
		$html .= "<a href='". $basedir ."index.php?page=$current_page'>No</a>";
		$html .= "<br/>\n";
		$html .= "<br/>\n";
		$html .= "<br/>\n";
		$html .= "<a href='". $basedir ."index.php?page=$current_page&action=userdelete2&un=$username'>YES</a>";
		$html .= "<br/>\n";
		$html .= "<br/>\n";
		$html .= "<br/>\n";
		
		return $this->templateAdminInterface->deleteUserConfirmation($html);
	}
	
	/**
	* print message for succesuful password change with old and new id
	*/
	function printPWChangeSuccessful(){
		$html = "";
	
		if(isset($_POST['oldpwid']) &&  isset($_POST['username']) ){
			$user = new TUser($_POST['username']);
			$oldid = $_POST['oldpwid'];
			$newid = $user->pwhash();
			$html .= "Old pwid:$oldid, new pwid: $newid.";
		}
		
		return $this->templateAdminInterface->printPWChangeSuccessful($html);
	}
	
	/**
	* generates link for password change
	* @param string $username username whose password change link is created
	* @param string $basedir url directory prefix for link creation
	*/
	private function generatePWChangeLink($username, $basedir=""){
		$current_page = $_GET['page'];
		#$hash = $this->userAdminController->showUserHash($username);
		return "$username </td><td> <a href='". $basedir ."index.php?page=$current_page&action=pwchange&un=$username'>[Change pw]</a></td><td> <a href='".$basedir."index.php?page=$current_page&action=userdelete&un=$username'>[Delete User]</a>"; 
	}
	
	/**
	* prints link to user creation form
	* @param string $basedir url directory prefix for link creation
	*/
	function createUserLink($basedir=""){
		$current_page = $_GET['page'];
		$html = "";
		$html .= "<a href='".$basedir."index.php?page=$current_page&action=createuser'>[Create New User]</a>";
		return $this->templateAdminInterface->createUserLink($html);
	}


}


?>
