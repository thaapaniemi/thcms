<?php

/**
* AdminTools index page
* @package THCMS\AdminTools
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

require_once("../config.php");
$pageAdminInterface = $GLOBALS['pageAdminInterface'];
$pageAdminController = $GLOBALS['pageAdminController'];
$userAdminInterface = $GLOBALS['userAdminInterface'];
$userAdminController = $GLOBALS['userAdminController'];


$login = $GLOBALS['loginController'];
session_start();
$login->handleLogin();

$html = "";
if($login->isPermissiontoRead("admin")){

// Admin Overview
$html .= "<html><body>\n\n";
$html .= "<h1><a href='index.php'>AdminTools</a></h1><br/>";
$html .= "<h2>Users:</h2><br/>\n";
$html .= "<p><font color='#0000FF'>Blue</font>: read-only group, <font color='#FF0000'>Red</font>: Read/write group permissions</p>";

$html .= $userAdminController->handlePOST();
$html .= $userAdminInterface->listAllUsers();

$html .= "<br/><br/>\n\n";
$html .= $userAdminInterface->CreateUserLink();

$html .= "<br/><br/>\n\n";

$html .= "<h2>Pages:</h2><br/>\n";
$html .= $pageAdminController->handlePOST();
$html .= $pageAdminInterface->listAllPages();

$html .= "\n\n</body></html>\n";

}else{
	$html .= "<html><body><p>Not authorized.</p></body></html>";
}




$pi = $GLOBALS['pageInterface'];
$template = $GLOBALS['templateHandler'];
//$template->addReplace('<!-- THCMS: -->', );

$template->addReplace('<!-- THCMS:TITLE -->', CMSTITLE);
$template->addReplace('<!-- THCMS:MENUTITLE -->', CMSTITLE);
$template->addReplace('<!-- THCMS:CONTENT -->', $html);

$template->addReplace('<!-- THCMS:NAVBAR_LEFT -->', $pi->printNavbar());
$template->addReplace('<!-- THCMS:NAVBAR_RIGHT -->', $pi->printPageTools($page) . $pi->printAdminTools() . $pi->navbarLoginTools() );

$template->addReplace('<!-- THCMS:POWEREDBY -->', $pi->printPoweredByText());

echo $template->getHTML();

?>
