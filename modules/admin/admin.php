<?php
/**
* AdminTools AdminTools Module class file
* @package THCMS\AdminTools
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/


require("padminController.php");
require("padminInterface.php");
require("uadminController.php");
require("uadminInterface.php");

/**
* AdminModule (AdminTools) for THCMS
*/
class adminModule{

	/**
	* Module default action
	* @return string HTML output
	*/
	public function run(){
	
		$current_page = $_GET['page'];
		
		$templateAdminInterface = $GLOBALS['templateAdminInterface'];
		$pageAdminInterface = new PageAdminInterface($templateAdminInterface );
		$pageAdminController = new PageAdminController($pageAdminInterface);
		$userAdminInterface = new UserAdminInterface($templateAdminInterface );
		$userAdminController = new UserAdminController($userAdminInterface);
		$login = $GLOBALS['loginController'];
		
		$html = "";
		if($login->isPermissiontoRead("admin")){ //Only admins have rights to use admin module
			// Admin Overview
		$html .= "<h2>Users:</h2><br/>\n";
		$html .= "<p><font color='#0000FF'>Blue</font>: read-only group, <font color='#FF0000'>Red</font>: Read/write group permissions</p>";

		$html .= $userAdminController->handlePOST();
		$html .= $userAdminInterface->listAllUsers();

		$html .= "<br/><br/>\n\n";
		$html .= $userAdminInterface->CreateUserLink();

		$html .= "<br/><br/>\n\n";

		$html .= "<h2>Pages:</h2><br/>\n";
		$html .= $pageAdminController->handlePOST();
		$html .= $pageAdminInterface->listAllPages();
		}else{
			$html .= "<p>Not authorized.</p>";
		}
		
		
		return $html;
	}

}

?>
