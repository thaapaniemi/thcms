<?php

/**
* AdminTools UserAdminController class file
* @package THCMS\AdminTools
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/


/**
* Controller class for userAdmin
* @package THCMS\AdminTools
*/
class UserAdminController{

	/**
	* userAdminInterface
	*/
	private $userAdminInterface = null;

	/**
	* constructor for UserAdminController
	* @param userAdminInterface $userAdminInterface uAi object
	*/	
	function __construct($userAdminInterface){
		$this->userAdminInterface = $userAdminInterface;
	}
	
	/**
	* destructor
	*/
	function __destruct() {}
	
	/**
	* Creates new user
	* @param string $username username for new user
	* @param string $pw password for new user
	*/
	function createUser($username, $pw){
		$user = new TUser($username, $pw);
	}
	
	/**
	* Deletes user from "database"
	* @param string $username username to be deleted
	*/
	function deleteUser($username){
		$user = new TUser($username);
		$user->delete();
	}
	
	/**
	* Changes user password
	* @param string $username username whose password is to be changed
	* @param string $newPW new password for that user
	* 
	*/
	function editUserPW($username, $newPW){
			$user = new TUser($username);
			$user->password($newPW);
	}

	/**
	* Handle GET/POST requests
	* 
	* @return string Responses to requests
	*/
	function handlePOST(){
		$userAdminInterface = $this->userAdminInterface;
		$html = "";
	
		if( isset($_GET['action']) ){
			if($_GET['action'] == 'pwchange2'){
				$this->editUserPW($_POST['username'], $_POST['pw']);
				$html .= $userAdminInterface->printPWChangeSuccessful();
			}
		else if($_GET['action'] == 'pwchange'){
			$html .= $userAdminInterface->changePasswordForm();
		}else if($_GET['action'] == 'createuser2'){
			$this->createUser($_POST['username'], $_POST['pw']);
		}
		else if($_GET['action'] == 'createuser'){
			$html .= $userAdminInterface->showUserCreationForm();
		}
		else if($_GET['action'] == 'userdelete'){
			$html .= $userAdminInterface->deleteUserConfirmation();
		}else if($_GET['action'] == 'userdelete2'){
			$this->deleteUser($_GET['un']);
		}else if($_GET['action'] == 'addgroup'){
			$html .= $userAdminInterface->showGroupAddingForm($_GET['username']);
		}else if($_GET['action'] == 'addgroup2'){
			$user = new TUser($_POST['username']);
			$user->setGroup($_POST['group'], $_POST['rw']);
		}else if($_GET['action'] == 'ungroup'){
			$user = new TUser($_GET['username'], false);
			$user->unsetGroup($_GET['group']);
		}else{
			//$a = $_GET['action'];
			//$html .= "action $a is no implemented.<br/>\n\n";
		}	
		}
	
		return $html;
	}

}


?>
