<?php

/**
* AdminTools PageAdminController class file
* @package THCMS\AdminTools

*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

/**
* Controller class for pageAdmin
* @package THCMS\AdminTools
*/
class PageAdminController{

	/**
	* $templateAdminInterface
	*/
	private $templateAdminInterface = null;

	/**
	* Constructor
	* @param templateAdminInterface $templateAdminInterface tAi object
	*/
	public function __construct($templateAdminInterface) {$this->templateAdminInterface = $templateAdminInterface;}
	
	/**
	* Destructor
	*
	*/
	public function __destruct() {}
	
	/**
	* Delete page
	* @param string $page Name of page wanted to delete
	*
	* Function calls pageDB to remove page from db.
	*/
	public function deletePage($page){
		$page = PageCache::getPage($page);
		$page->delete();
	}
	
	/**
	* Handle GET/POST requests
	* 
	* @return string Responses to requests
	*/
	public function handlePOST(){
		$html = "";
		if( isset($_GET['action']) ){
			if($_GET['action'] == 'remove' && isset($_GET['removepage']) ){
				$this->deletePage($_GET['removepage']);
			}else if($_GET['action'] == 'dconfirm' && isset($_GET['removepage']) ){
				$html .= $this->templateAdminInterface->deleteConfirmation($_GET['removepage']);
			}
		}
		
		return $html;
	}
	
}


?>
