<?php
/**
* TCHMS Module support class
* @package THCMS
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

require_once(BASEDIR . "/3rdparty/lib.uuid.php");

/**
* Class declaration for Module support
* @package THCMS\ModuleSupport
*/
class THCMSModule{

	/**
	* Pattern variables
	*/
	private $startPattern, $stopPattern, $functionSeparator = null;
	
	/**
	* list of loaded modules
	*/
	private $modules = array();
	
	/**
	* list of tasks
	*/
	private $tasks = array();

	/**
	* Constructor
	* @param startPattern string modulePattern starts here
	* @param stopPattern string modulePattern stops here
	*/	
	public function __construct($startPattern, $stopPattern){
		$this->startPattern = $startPattern;
		$this->stopPattern = $stopPattern;
		$this->functionSeparator = ":";
	}
	
	/**
	* Run moduleSupport (get names-> load modules -> run tasks)
	* @param $html string containing module names
	* @return string Task output html
	*/
	public function run(&$html){
		try{
			$this->getModulesFromString($html);
			$this->loadModules();
			$this->replacePlaceholdersWithOutput($this->runTasks(), $html);
		}catch(RuntimeException $e){
			$html = $e->getMessage();
		}
	}

	/**
	* Load module names and task names from string to arrays
	* @param $html string containing module names
	* @return false (recursion, modifications goes to $html)
	*/
	private function getModulesFromString(&$html){
			$beginning = strpos($html, $this->startPattern);
			$end = strpos($html, $this->stopPattern);
			
			if(strpos($html, $this->startPattern) && strpos($html, $this->stopPattern)){
				$value = substr($html, $beginning, ($end - $beginning));			
				$value = str_replace($this->startPattern, "", $value);
				
				$needle = $this->startPattern . $value . $this->stopPattern;
				$pos = strpos($html,$needle);
				$uuid = UUID::mint( 4 );
				if ($pos !== false) {
			    	$html = substr_replace($html,"<!-- $uuid -->",$pos,strlen($needle));
				}
				
				
				// TODO: Better sanitation
				$value = str_replace(";", "", $value);
				$value = str_replace('"', "", $value);
				
				$value = explode($this->functionSeparator, $value);
				
				$module = $value[0];
				$this->modules[] = array("module"=>$module, "uuid"=>$uuid);
				
				if(!isset($this->tasks[$module])){
					$this->tasks[$module] = array();
				}
				
				unset($value[0]);
				
				if(sizeof($value) >= 1){
					foreach($value as $task){
						$this->tasks[$module][] = array("task"=> $task, "uuid"=>$uuid);
					}
				}else{
					$this->tasks[$module][] = array("task"=> "run()", "uuid"=>$uuid);
				}
				
				//recursion test
				return $this->getModulesFromString($html);
			}else{
				return false;
			}
	}
	
	/**
	* Create objects from names in array
	* @throws RuntimeException
	*/
	private function loadModules(){
		$moduleObjects = array();
		foreach($this->modules as $modulearray){
			$module = $modulearray["module"];
			
			if (file_exists(BASEDIR . "/modules/$module/$module.php")){
				include_once("$module/$module.php");
			}else{
				throw new RuntimeException("Modulefile for $module not found");
			}
			
			$mm = array();
			$mm["uuid"] = $modulearray["uuid"];
			if(class_exists($module . "Module")){
				eval('$mm["module"] =' . "new ". $module . "Module();" );
			}else{
				throw new RuntimeException("Module class ".$module."Module not found");
			}
			$moduleObjects[$module] = $mm;
		}
		
		$this->modules = $moduleObjects;
	}
	
	/**
	* Run tasks associated to modules
	* @return string task output (return values);
	*/
	private function runTasks(){
		$output = array();
		foreach($this->tasks as $module=>$tasks){
			foreach($tasks as $task){
			
				$split = explode("(", $task["task"])[0];
				
				if(method_exists($this->modules[$module]["module"], $split)){
					eval('$t= $this->modules[$module]["module"]->'.$task["task"].';');
				}else{
					throw new RuntimeException("Method " .$task["task"]. " not found");
				}
				
				if(isset($task["uuid"])){
						$output[(string)$task["uuid"]][] = $t;
				}else{
					$output[(string)$task["uuid"]] = array($t);
				}
			}
		}
		
		return $output;
	}
	
	
	/**
	* Replaces uuid comments with associated output values
	* @param $output array of uuid->output pairs
	* @param $html string where replaced
	*/
	private function replacePlaceholdersWithOutput($output, &$html){
		foreach($output as $key=>$value){
			$valueout = "";
			foreach($value as $v){
				$valueout .= $v;
			}
			$html = str_replace("<!-- $key -->", $valueout, $html);
		}
	}

}


?>
