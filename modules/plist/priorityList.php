<?php
/**
* TCHMS priorityList module
* @package THCMS\Modules
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

require_once(BASEDIR . "/core/KV.php");

/**
* TCHMS Module priorityList class declaration

* @package THCMS\Modules
*/
class prioritylist{
	
	/**
	* UUID identification number
	*/
	private $uuid = null;
	
	/**
	* Array of list items
	*/
	private $values = null;
	
	/**
	* Ischanged boolean variable for determining need to save
	*/
	private $ischanged = false;
	
	
	/**
	* Constructor
	* @param string $uuid identification
	*/
	public function __construct($uuid){
		$temp = KV::get($uuid);
		$this->uuid = $uuid;
		
		if($temp == false){
			$temp['valueType'] = 'priorityList';
			$temp['content'] = array();
		}
		
		if($temp){
			$temp = (array)$temp;
			if((string)$temp['valueType'] == 'priorityList'){
				$this->values = (array)$temp['content'];
			}else{
				throw new RuntimeException("plist KV key mismatch to content");
			}
		}else{
			$this->values = array();
		}
	}
	
	/**
	* Destructor that saves values to db
	*/
	public function __destruct(){
		if($this->ischanged = true){
			$temp = array();
			$temp['valueType'] = 'priorityList';
			$temp['content'] = $this->values;
			KV::set($this->uuid, $temp);
		}
	}
	
	/**
	* Get all list items
	* @return array List items
	*/
	public function values(){
		return $this->values;
	}
	
	/**
	* Swap placese with two array items
	* @param int $id1 id whose content shall be swapped with the another
	* @param int $id2 id whose content shall be swapped with the another
	*/
	public function swap($id1, $id2){
		$temp = $this->values[$id1];
		$this->values[$id1] = $this->values[$id2];
		$this->values[$id2] = $temp;
		$this->ischanged = true;
	}
	
	/**
	* Delete item from db
	* @param int $id Identification
	*/
	public function delete($id){
		unset($this->values[$id]);
		sort($this->values);
	}
	
	/**
	* Add new item to list
	* @param string $content list to save
	*/
	public function add($content){
		$this->values[] = $content;
	}
	
	/**
	* Change content of list object
	* @param int $id ID
	* @param string $content New content for id
	*/
	public function change($id, $content){
		$this->values[$id] = $content;
	}
	
	/**
	* Clear whole priorityList
	*/
	public function clear(){
		$this->values = null;
		$this->values = array();
	}


}

?>
