<?php
/**
* TCHMS priorityList module
* @package THCMS\Modules
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

include("priorityList.php");


/**
* TCHMS Module plistModule class declaration

* @package THCMS\Modules
*/
class plistModule{
	
	/**
	* priorityList object
	*/
	private $pl = null;
	
	/**
	* default constructor, loads default list, id hardcoded
	*/
	public function __construct(){
		#$this->pl = new priorityList('b0838449-7184-4a80-9d27-221bb27e21f4');
	}
	
	/**
	* Create empty list
	*
	*
	*/
	public function createNew(){
		$uuid = UUID::mint(4);
		while( KV::get($uuid) != false ){
			$uuid = UUID::mint(4);
		}
		
		$defcontent = "::[plist:run('$uuid')]::" . "\n"
		.'::[plist:handleForm()]::' . "\n"
		.'::[plist:addForm()]::' . "\n"
		.'::[plist:printTable()]::'. "\n";

		
		$this->pl = new priorityList($uuid);
		$page = PageCache::getPage($GLOBALS['page']);
		$page->content( str_replace("::[plist]::", "", $page->content()) );
		$page->content($page->content() . $defcontent);
	}
	
	/**
	* Module run method
	* @param string $uuid id of wanted list
	*/
	public function run($uuid=null){
		if(isset($uuid)){
			$this->pl = new priorityList($uuid);
		}else{
			$this->createNew();
		}
	}


	/**
	* print content of list
	* @return string HTML code
	*/
	public function printTable(){
		$page = $GLOBALS['page'];
		$pl = $this->pl;
		
		$values = $pl->values();
		$h = array();
		
		
		foreach($values as $k => $v){
			$hh = array();
			
			if(isset($values[$k+1])){
				$hh[] = "<a href='index.php?page=$page&id=$k&id2=" .($k+1). "&action=lower'><i class='icon-chevron-down'></i></a>";
			}else{
				$hh[] = "<i class='icon-minus'></i>";
			}
			if(isset($values[$k-1])){
				$hh[] =  "<a href='index.php?page=$page&id=$k&id2=" .($k-1). "&action=higher'><i class='icon-chevron-up'></i></a>";
			}else{
				$hh[] = "<i class='icon-minus'></i>";
			}
			
			$hh[] = $v;
			$hh[] = "<a href='index.php?page=$page&id=$k&action=editOld'> [Edit] </a>";
			
			#$html .= "<a href='index.php?page=$page&id=$id&action=toTop'> to Top </a>";			
			#$html .= "<a href='index.php?page=$page&id=$id&action=toBottom'> to Bottom </a>";
			#$html .= "<a href='index.php?page=$page&id=$id&action=delete'> [Delete] </a>";
			
			$h[] = $hh;
		}
		
		#$html .= "</table>\n";
		
		$html = $this->teeTaulu($h);
		
		return $html;
	
	}
	
	/**
	* Handle GET/POST actions sent with form or links
	*/
	public function handleForm(){
		$pl=$this->pl;
	
		if(isset($_REQUEST['action'])){
			$content = null;
			$id = null;
			$id2 = null;
			
			if(isset($_REQUEST['id'])){
				$id = $_REQUEST['id'];
			}
			if(isset($_REQUEST['id2'])){
				$id2 = $_REQUEST['id2'];
			}
			
			
			if($_REQUEST['action'] == 'addNew' && isset($_REQUEST['content']) ){
				$content = $_REQUEST['content'];
				
				if(!isset($_REQUEST['id'])){
					$pl->add($content);
				}else{
					$pl->change($_REQUEST['id'], $content);
				}
			}
			
			$pl = $this->pl;			
			
			if($_REQUEST['action'] == 'higher'){
				$pl->swap($id, $id2);
			}else if($_REQUEST['action'] == 'lower'){
				$pl->swap($id, $id2);
			}else if($_REQUEST['action'] == 'toTop'){
#				$po->toTop();
			}else if($_REQUEST['action'] == 'toBottom'){
#				$po->toBottom();
			}else if($_REQUEST['action'] == 'delete'){
				$pl->delete($id);
			}else if($_REQUEST['action'] == 'clear'){
				$pl->clear();
			}
			
			if($_REQUEST['action'] == 'createNewList'){
				$this->createNew();
			}
			
		}
	}
	
	/**
	* Print form for adding new list value
	* @return string HTML code
	*/
	public function addForm(){
		$html  = "";
		$id = null;
		$content = null; 
		$priority = null;
		
		
		if(isset($_REQUEST['action']) && isset($_REQUEST['id'])){
			if($_REQUEST['action'] == 'editOld'){
				$page = $_GET['page'];
				$html .= "<a href='index.php?page=$page'> Cancel edit </a><br/>";
				$id = $_REQUEST['id'];
				
				$isok = false;
				$o = $this->pl->values()[$id];
				
				$content = $o;
				$priority = $id;
				
				$html .= "<br/><a href='index.php?page=$page&id=$id&action=delete'> Delete $content </a><br/>";
			}
		}
		
		$page = $_REQUEST['page'];
		
		$html .= "<form name='plistAdd' action='index.php?page=$page' method='POST'>";
		#$html .= "<input name='content' type='text' placeholder='Content' value='$content' />";
		#$html .= "<input name='priority' type='text' placeholder='Priority' value='$priority' /><br/>\n";
		$html .= "<textarea name='content' rows='3' placeholder='Content'>$content</textarea><br/>\n";
		$html .= "<input name='action' type='hidden' value='addNew'/>";
		if(isset($id)){$html .= "<input name='id' type='hidden' value='$id'/>";}
		$html .= "<input id='lisays' type='submit' name='submit' value='Send' />";
		$html .= "</form>";
		
		return $html;
	}
	
	
	/**
	* Transforms array to table
	* todo: change language
	* @param array $sisalto Content of table
	* @return string HTML code
	*/
	private function teeTaulu($sisalto){
	$taulu = "<table>";
	foreach($sisalto as $key=>$value){
		
		if($key % 2 == 0){
			$taulu .="<tr bgcolor='#EEEEEE'>\n";
		}else{
			$taulu .="<tr bgcolor='#FFFFFF'>\n";
		}
		
		foreach($value as $k => $v){
			     if($k == 0){$taulu .= "<td style='width:5%; align:center;'>";}
			else if($k == 1){$taulu .= "<td style='width:5%; align:center;'>";}
			else if($k == 2){$taulu .= "<td style='width:80%; align:left;'>";}
			else if($k == 3){$taulu .= "<td style='width:10%; align:right;'>";}
			#$taulu .= "<td>";
			$taulu .= $v;
			$taulu .= "</td>\n";
		}
		$taulu .= "</tr>";
	}
	$taulu .= "</table>";
	return $taulu;
}

}


?>
