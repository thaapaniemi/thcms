<?php
/**
* TCHMS Module support test module file
* @package THCMS
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

//////////////////////////////

/**
* Class declaration for testModule
* @package THCMS\ModuleSupport
*/
class testModule{
	
	/**
	* Default action when running module
	* @return string html-compatible output of testModule
	*/
	public function run(){
		return "Hello world. This is loaded from test module<br/>\n";
	}
	
	/**
	* test method
	* @return string html-compatible output of testModule
	*/
	public function lol(){
		return "This is returned from test module function lol.<br/>\n";
	}
	
	/**
	* test method
	* @param $mm string argument test
	* @return string $mm +<br>\n
	*/	
	public function lol2($mm){
		return "$mm.<br/>\n";
	}
	
}

?>
