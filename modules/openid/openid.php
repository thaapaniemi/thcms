<?php
/**
* TCHMS OpenID module class declaration file
* @package THCMS
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/


require_once(BASEDIR . '/3rdparty/lightopenid/openid.php');


/**
* openidModule class declaration
* @package THCMS\Modules
*/
class openidModule{

	/**
	* Default action
	* @return string $html HTML output
	*/
	public function run(){
		$page = $GLOBALS['page'];
		$html = "";
		$user = new TUser($GLOBALS['loginController']->username());
		$id = $user->openid();
		
		if(!isset($id)){
			$html .= '<form action="index.php?page='.$page.'&openidlogin=true" method="post"><button>Add Google identity</button></form>';
		}else{
			$html .= '<form action="index.php?page='.$page.'&openidremove=true" method="post"><button>Remove Google identity</button></form>';
		}
		
		$html .= $this->handleRequest();
		
		return $html;
	}
	
	
	/**
	* Handle POST request to openID handling
	* return string for succesful login
	*/
	public function handleRequest(){
		if(isset($_GET['openidremove'])){
			$user = new TUser($GLOBALS['loginController']->username());
		    $user->removeOpenid();
		}
		
		try {
		    # Change 'localhost' to your domain name.
		    $openid = new LightOpenID(DOMAIN);
		    if(!$openid->mode) {
        		if(isset($_GET['openidlogin'])) {
	        	    $openid->identity = 'https://www.google.com/accounts/o8/id';
    	    	    header('Location: ' . $openid->authUrl());
    	    	}
	        }elseif($openid->mode == 'cancel') {
		        return 'User has canceled authentication!';
		    } else {
		    	
		    	if($openid->validate()){
		    		#var_dump($openid->validate());
		    		#var_dump($GLOBALS['loginController']->username());
		    		#var_dump($openid->identity);
		    		$user = new TUser($GLOBALS['loginController']->username());
		    		$user->openid($openid->identity);
		    		return $openid->identity . " added to " . $GLOBALS['loginController']->username() . "<br/>\n";
		    	}
		    
        		return 'User ' . ($openid->validate() ? $openid->identity . ' has ' : 'has not ') . 'logged in.';
		    }
        
        }catch(ErrorException $e) {
 			return $e->getMessage();
		}
	}
}

?>
