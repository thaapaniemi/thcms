<?php

/**
* TCHMS Internal configuration file
* @package THCMS
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
* Edit this for your needs. Here should be more or less advanced configuration data for thcms.
*/

define('VERSION','0.24');
define('BASEDIR', dirname(__FILE__));

require_once("core/pageCache.php");
require_once("core/pageCache.php");
require_once(BASEDIR . '/3rdparty/idiorm/idiorm.php');

require_once(BASEDIR . '/modules/moduleSupport.php');

// Enable if want to show login form without ssl (login still uses ssl)
$overrideSSLCheck = true;

// sniff when ssl in use and change current_url to use it.
$http = 'http://';
$sslConnection = false;
if($_SERVER['HTTPS'] != ""){
	$http = 'https://';
	$sslConnection = true;
}
$current_url= $http . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'];
$current_url_ssl="https://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']; // for login

//TOR hidden service "fix", we don't need https inside tor.
if(strpos($_SERVER['HTTP_HOST'], ".onion")){
	$overrideSSLCheck = true;
	$current_url_ssl="http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'];
}

$current_url_ssl= str_replace("/index.php", "/", $current_url_ssl);
$current_url= str_replace("/index.php", "/", $current_url);
$current_url= str_replace("/install.php", "/", $current_url);

$idiorm_statements = $GLOBALS['idiorm_statements'];
foreach($idiorm_statements as $statement){
	ORM::configure($statement);
}

// LoginController
#require_once("core/userDB_mysql.php");
require_once("core/userDB_idiorm.php");
require_once("3rdparty/lightopenid/openid.php");
require_once("core/loginController.php");

$loginController = new LoginController();

//GroupDB
require_once("core/group_idiorm.php");


//Template support
require_once("3rdparty/simple_html_dom.php");

#require_once("core/CMSTemplate.php");
require_once("templates/thtp.php");
$templateInterface = null;
$templateAdminInterface = null;


if(file_exists(BASEDIR ."/templates/$template/$template.tmpl")){
	THTP::load( file_get_contents(BASEDIR ."/templates/$template/$template.tmpl") );
	require_once("templates/$template/" . $template ."_interface.php");
	require_once("templates/$template/" . $template ."_admin_interface.php");
	$templateInterface = new TemplateInterface();
	$templateAdminInterface = new TemplateAdminInterface();
}else{
	# Minimal Safety laoded template
	THTP::load("<html><body>\n<!-- THCMS:NAVBAR_RIGHT --><br/>\n<!-- THCMS:NAVBAR_LEFT --><br/>\n<!-- THCMS:CONTENT -->\n</body></html>");
}


THTP::addReplace('<!-- THCMS:TEMPLATECSS -->', $current_url . "templates/$template/$template.css");
THTP::addReplace('<!-- THCMS:TEMPLATEDIR -->', $current_url . "templates/$template/");

// PageController
require_once("other/parserWrapper.php");
#require_once("core/pageDB_mysql.php");
require_once("core/pageDB_idiorm.php");
require_once("core/pageController.php");
require_once("core/pageInterface.php");

$pageController = new PageController();
$pageInterface = new PageInterface( $loginController, $templateInterface, new parserWrapper() );

/*
//Admin tools
require_once("admin/padminController.php");
require_once("admin/padminInterface.php");
require_once("admin/uadminController.php");
require_once("admin/uadminInterface.php");
$pageAdminController = new PageAdminController();
$pageAdminInterface = new PageAdminInterface($templateAdminInterface );
$userAdminDB = null;
$userAdminController = new UserAdminController();
$userAdminInterface = new UserAdminInterface($templateAdminInterface );
*/
try{
	THTP::addReplace('<!-- THCMS:TITLE -->', CMSTITLE);
	THTP::addReplace('<!-- THCMS:MENUTITLE -->', CMSTITLE);
	THTP::addReplace('<!-- THCMS:SIDEBAR -->', $pageInterface->readPage(SIDEBAR, true)); //SIDEBAR parse must be first (phpsimpledom thingie)
	THTP::addReplace('<!-- THCMS:BASEDIR -->', $GLOBALS['current_url']);
	THTP::addReplace('<!-- THCMS:NAVBAR_LEFT -->', $pageInterface->printNavbar(NAVBAR));
	#THTP::addReplace('<!-- THCMS:NAVBAR_RIGHT -->', $pageInterface->printPageTools($page) . $pageInterface->printAdminTools() . $pageInterface->navbarLoginTools() );
	THTP::addReplace('<!-- THCMS:PAGELASTMODIFIED -->',$pageInterface->printPageLastModified($page));
	THTP::addReplace('<!-- THCMS:POWEREDBY -->', $pageInterface->printPoweredByText());
}catch(PDOException $e){/* This happens in installation for example... */}

?>
