<?php

/**
* TCHMS Wrapper and extension for Wiki parser
* @package THCMS
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

// www.simplewiki.com Parser
require_once(BASEDIR . "/3rdparty/simplewiki1_2/muster/simplewiki.php");
require_once(BASEDIR . "/3rdparty/simplewiki1_2/muster/simplewiki/docnode.php");
require_once(BASEDIR . "/3rdparty/simplewiki1_2/muster/simplewiki/emitter.php");
require_once(BASEDIR . "/3rdparty/simplewiki1_2/muster/simplewiki/parser.php");
require_once(BASEDIR . "/3rdparty/simple_html_dom.php");
require_once(BASEDIR . "/modules/moduleSupport.php");

/**
* Class declaration for wikiParserWrapper
* @package THCMS\Parser
*/
class parserWrapper{

	/**
	* object for real parser
	*/
	private $realParser = null;

	/**
	* Constructor
	*
	* Formats parser to working condition.
	*/
	function __construct(){
	
		$this->realParser = new Muster\SimpleWiki();
	}
	
	/**
	* Parsing function.
	*
	* Wraps real parse() and extends it a little bit.
	*
	* @param string $input Wiki-syntaxed input
	* @return string HTML-syntaxed output
	*/
	function parse($input){
		$module = new THCMSModule("::[", "]::");
		$data = $this->realParser->get_html($input);
		
		//Convert SimpleWikiparsers links to be usable with this
		$html = str_get_html($data);
		
		if($html != false){
		foreach($html->find('a') as $element){
			if(!strpos($element, "http")){
			$url = $element->href;
			$url='index.php?page=' . $url;
			$element->href=$url;
		
		}else{
			continue;
		} //for external links
		}
		
		$module->run($html);
		$html = (string) $html;
		
		return $html;
		
		}else{
			return ""; // This is for empty sidebar
		}
	}
}



?>
