<?php
/**
* TCHMS PageDB class declaration file
* @package THCMS
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

require_once(BASEDIR . '/3rdparty/idiorm/idiorm.php');

/**
* (true)Page class declaration for thcms
*
* TPage is container for page related methods. It cannot be used without
* first configuring IdiORM (Done in PageDB). Naming because DB class is probably name Page
*/
class TPage{
	
	/**
	* Page object from ORM
	*/
	private $page = null;
	
	/**
	* Is this new page or loaded from DB?
	*/
	private $isnew = false;
	
	/**
	* Is Content of this page changed?
	*/
	private $ischanged = false;
	
	
	/**
	* Get all pages
	*/
	public static function getAllPages(){
		try{
			$pages0 = ORM::for_table(PAGECLASS)->select('id')->order_by_asc('id')->find_many();
			$pages = array();
			foreach($pages0 as $p){
				$pages[] = $p->id;
			}
			return $pages;
		}catch(Exception $e){
	  	echo "TPage:getAllPages error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; }
	}
	
	/**
	* Private Constructor, use through open-method
	* 
	* @param string $pagename Name of page wanted to open
	* @param boolean $createnew If page doesn't exists shall we make new?
	*/
	private function __construct($pagename, $createnew) {
	try{
		$loadedpage = ORM::for_table(PAGECLASS)->where('id', $pagename)->find_one();
		
		if(!$loadedpage && $createnew){
			$this->page = ORM::for_table(PAGECLASS)->create();
			$this->page->id = $pagename;
			$this->page->content = "";
			$this->page->last_modified = null;
			$this->page->parse = true;
			$this->page->group = 'none';
			
			$this->isnew = true;
			$this->ischanged = true;
		}else if($loadedpage){
			$this->page = $loadedpage;
			$this->isnew = false;
		}
		
	}catch(Exception $e){
	  	echo "TPage:Constructor error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; }	
	}


	/**
	* Open a Page method
	* 
	* @param string $pagename Name of page wanted to open
	* @param boolean $createnew If page doesn't exists shall we make new?
	*/
	public static function open($pagename, $createnew = true){
		$loaded = new TPage($pagename, $createnew);
		
		if($loaded->isnew){
			if(!$createnew){ return null;}
		}
		return $loaded;
	}
	
	/**
	* Combined Getter/setter for Name
	* @param $new string new value
	* @return String name
	*/
	public function name($new=null){
		if(isset($new)){
			$this->page->id = $new;
			$this->ischanged = true;
		}
		
		return $this->page->id;
	}
	
	/**
	* Combined Getter/setter for Content
	* @param $new string new value
	* @return String Content
	*/
	public function content($new=null){
		if(isset($new)){
			$this->page->content = $new;
			$this->ischanged = true;
		}
		
		return $this->page->content;
	}
	
	/**
	* Combined Getter/setter for last_modified
	* @param $new string new value
	* @return String last_modified
	*/
	public function lastModified($new = null){
		if(isset($new)){
			$this->page->last_modified = $new;
			$this->ischanged = true;
		}
		return $this->page->last_modified;
	}

	/**
	* Combined Getter/setter for Parse
	* @param $new boolean new value
	* @return boolean parse
	*/
	public function parse($new=null){
		if(isset($new)){
			if($new){$new = 1;}else{$new = 0;}
			$this->page->parse = $new;
			$this->ischanged = true;
		}
	
		return (boolean) $this->page->parse;
	}
	
	/**
	* Getter for isNew
	* @return boolean isNew
	*/
	public function getIsNew(){
		if(!$this->page){return;}
	
		return (boolean) $this->page->isNew;
	}

	/**
	* Combined Getter/setter for page group
	* @param $newgroup string new value
	* @return string page group
	*/
	public function group($newgroup = null){
		if(isset($newgroup)){
			$this->page->group = $newgroup;
		}
		return $this->page->group;
	}
	
	/**
	* Delete page
	*/
	public function delete(){
		$this->page->delete();
		$this->page = null;
	}

	/**
	* ToString method
	*/	
	public function __toString()
    {
    
    	// For not retuning null	
    	$rvalue = "" . $this->content();
    	
    	return $rvalue;
    }
    
   	/**
	* Setter for ischanged
	* @param Boolean ischanged
	* 
	* This is mostly for not saving pages that are created without wanting to create new ones
	*/
    public function setChange($ischanged){
    	$this->ischanged = $ischanged;
    }
	
	
	/**
	* Destructor
	*
	* In the end save.
	*/
	function __destruct() {
		if( $this->ischanged  && isset($this->page) && isset($this->page->id)){
			$this->page->save();
		}
	}
	
	/**
	* Create tables needed for pageDB
	*
	* Creates new table pages reserved for pageDB use. Used from install.php
	*/
	static public function createTables(){
	try{
		$db = ORM::get_db();
		
		$sql= "CREATE TABLE IF NOT EXISTS ".PAGECLASS." (
			`id` varchar(255) NOT NULL,
			`content` longtext NOT NULL,
			`last_modified` datetime DEFAULT NULL,
			`parse` tinyint(1) NOT NULL,
			`group` varchar(255) DEFAULT NULL,
			PRIMARY KEY (`id`)
			)";
		
		$ss = $db->prepare($sql);
		$ss->execute();
	}catch(Exception $e){
	  	echo "TPage:createTables error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; }
	}
	
	/**
	* check if page exists in db
	* @param $pagename string Name of pageDB
	* return boolean does page exists
	*/
	static public function exists($pagename){
	  try{
		if(!ORM::for_table(PAGECLASS)->where('id', $pagename)->find_one()){
			return false;
		}else{
			return true;
		}
	  }catch(Exception $e){
	  	echo "TPage:exists error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; }
	}
  
}

?>
