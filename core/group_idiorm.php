<?php
/**
* TCHMS TGroup class declaration file
* @package THCMS
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

////////////////

/**
* TGroup class declaration
*
* THCMS User class for wrapping functionality for Group-ORMClass
* @package THCMS\DB
*/
class TGroup{
	
	/**
	* ORM class object
	*/
	private $ormgroup = null;
	
	/**
	* Is data changed in object?
	*/
	private $ischanged = false;
	
	/**
	* Constructor
	* @param $uid string Userid
	* @param $gname string group name
	* @param $rw string read/write permissions, default read
	* @param $ormobject ORMClass Idiorm object for direct association
	*/
	public function __construct($uid, $gname, $rw="r", $ormobject=null){
	try{
		if(isset($ormobject)){
			$this->ormgroup = $ormobject;
		}else{
			$g = ORM::for_table(GROUPCLASS)->where('user', $uid)->where('group', $gname)->find_one();
			if(!$g){
				$g = $new = ORM::for_table(GROUPCLASS)->create();
				$g->id = UUID::mint(4);
				$g->user = $uid;
				$g->group = $gname;
				$g->rw = $rw;
				$this->ischanged = true;
			}
			$this->ormgroup = $g;
		}
	}catch(Exception $e){
	  	echo "TGroup:Constructor error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; }
	}
	
	/**
	* Destructor
	*/
	public function __destruct(){
		try{
		if( isset($this->ormgroup) && $this->ischanged = true ){
			$this->ormgroup->save();
		}
		}catch(Exception $e){
	  	echo "TGroup:Destructor error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; }
	}
	
	/**
	* Getter/Setter for read/write permissions
	* @param string $new new Value
	* @return string "r" or "w"
	*/
	public function rw($new=null){
		if(isset($new)){
			$this->ormgroup->rw = $new;
			$this->ischanged = true;
		}
		
		return $this->ormgroup->rw;
	}
	
	/**
	* Save data to DB
	*/
	public function save(){
		try{
			$this->ormgroup->save();
			$this->ischanged = false;
		}catch(Exception $e){
	  	echo "TGroup:save error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; }
	}
	
	/**
	* Delete record from DB
	*/
	public function delete(){
		try{
			$this->ormgroup->delete();
			$this->ormgroup = null;
			$this->ischanged = false;
		}catch(Exception $e){
	  	echo "TGroup:delete error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; }
	}
	
	/**
	* ToString method
	* @return string groupname
	*/
	public function __toString(){
		return $this->ormgroup->group;
	}
	
	/**
	* Deletes all groups associated to User
	* @param string $user username
	*/
	static public function deleteAllGroupsOfUser($user){
		try{
			$groups = ORM::for_table(GROUPCLASS)->where('user', $user)->find_many();
			foreach($groups as $group){
				$group->delete();
			}
		}catch(Exception $e){
	  	echo "TGroup:deleteAllGroupsOfUser error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; }
	}
	
	/**
	* Get all group names in array
	* @return array Group names
	*/
	static public function getAllGroups(){
		try{
                $ormgroups = ORM::for_table(GROUPCLASS)->distinct()->select('group')->find_many();
                $groups = array();
                
                foreach($ormgroups as $g){
                        $groups[] = $g->group;
                }
                
                return $groups;
		}catch(Exception $e){
		  	echo "TGroup:getAllGroups error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; }
	}
        
	/**
	* Create tables to DB, for installation purposes
	*/
	static public function createTables(){
	  try{
		$db = ORM::get_db();
		$sql= "CREATE TABLE IF NOT EXISTS " . GROUPCLASS ." (
			  `id` varchar(255) NOT NULL,
			  `user` varchar(255) NOT NULL,
			  `group` varchar(255) NOT NULL,
			  `rw` set('r','w') NOT NULL,
			  PRIMARY KEY (`id`)
			  );";
		$ss = $db->prepare($sql);
		$ss->execute();
     }catch(Exception $e){
	  	echo "TGroup:createTables error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; }
   }
}

?>
