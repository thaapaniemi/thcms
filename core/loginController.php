<?php
/**
* TCHMS loginController class declaration file
* @package THCMS\Core
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

////.....

/**
* TCHMS loginController class declaration
*
* @package THCMS\Core
*/
class LoginController{
	
	/**
	* Constructor
	*/
	public function __construct() {}
	
	/**
	* Destructor
	*/
	public function __destruct() {}
	
	/**
	* Log user in if authenticated
	* @param string $username Username
	* @param string $password Password
	* @return boolean True if login was ok, else False
	*/
	public function login($username, $password){
		$user = new TUser($username);
		
		if( $user->authenticate($password) ){
			$_SESSION['loggedin'] = true;
			$_SESSION['username'] = (string)$user;
			session_regenerate_id();
			return true;
	   }else{ return false; }
	}
	
	/**
	* Log user in if identity found
	* @param string $id openID identity
	* @return boolean True if login was ok, else False
	*/
	public function loginOpenID($id){
		$user = TUser::loginWithOpenID($id);
		
		if($user){
			$_SESSION['loggedin'] = true;
			$_SESSION['username'] = (string)$user;
			session_regenerate_id();
			return true;
		}else{
			return false;
		}
	}
	
	
	
	/**
	* login handling (get POST values)
	*/
	public function handleLogin(){
		// Handle login with userename/pw
		if(isset($_POST['login_request'])){
			if(isset($_POST['username']) && isset($_POST['password'])){
				$this->login($_POST['username'], $_POST['password']);
			}
			// Handle login with OpenID
		}else if(isset($_GET['openidlogin_request'])){
			
			$openid = $this->handleOpenIDRequest();
			if($openid){
				$this->loginOpenID($openid);
			}
		}
	}
	
	/**
	* openID (google) Login handler
	*/
	public function handleOpenIDRequest(){
		try {
		    # Change 'localhost' to your domain name.
		    $openid = new LightOpenID(DOMAIN);
		    if(!$openid->mode) {
	        	    $openid->identity = 'https://www.google.com/accounts/o8/id';
    	    	    header('Location: ' . $openid->authUrl());
	        }elseif($openid->mode == 'cancel') {
		        return 'User has canceled authentication!';
		    } else {
		    	
		    	if($openid->validate()){
		    		return $openid->identity;
		    	}
		    
        		return false;
		    }
        
        }catch(ErrorException $e) {
 			echo $e->getMessage();
		}
	}
	
	/**
	* Is user logged in?
	* @return boolean True if is, else False
	*/
	public function isLogged(){
		if(isset($_SESSION['loggedin'])){
			return true;
		}return false;
	}
	
	/**
	* Does user have permission to view page?
	* @param string $group group of page
	* @param string $rw "r" for readOnly, "w" for "read/write"
	* @return boolean True if is, else False
	*/
	public function isPermissiontoRead($group, $rw="r"){
		$user = new TUser("none");
		if(isset($_SESSION['username'])){
			$user = new TUser($_SESSION['username']);
		}
		
		return $user->isPermission($group, $rw);
	}
	
	/**
	* Returns username
	* @return string username
	*/
	public function username(){
		if($this->isLogged()){
			return $_SESSION['username'];
		}else{
			return "None";
		}
	}
	
	/**
	* Logout handling (end session)
	*/
	static public function logout(){
		session_unset();
	}

}

?>
