<?php
/**
* TCHMS pageInterface class declaration file
* @package THCMS\Core
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

////.....

/**
* TCHMS pageInterface class declaration
*
* @package THCMS\Core
*/
class PageInterface{

	/**
	* loginController object
	*/
	private $loginController = null;
	
	/**
	* parser object
	*/
	private $parser = null;
	
	/**
	* TemplateInterface object
	*/
	private $templateInterface = null;
	
	/**
	* Constructor 
	* @param loginController $loginControllerontroller Object for loginController
	* @param templateInterface $templateInterface Object for templateInterface
	* @param parser $parser Parser in use
	*/
	function __construct($loginControllerontroller, $templateInterface, $parser) {
		$this->loginController = $loginControllerontroller;
		
		$this->templateInterface = $templateInterface;
		
		if(isset($this->templateInterface)){
			$this->templateInterface->setLoggedInStatus($this->loginController->isLogged());
		}

		$this->parser = $parser;
	}
	
	/**
	* Destructor
	*/
	function __destruct() {
		$this->loginController = null;
		$this->templateInterface = null;
		$this->parser = null;
	}
	
	/**
	* Read page from db
	* @param string $page Name of page wanted
	* @param boolean $parse Parse page? (Default: true)
	* @return string HTML string
	*/
	function readPage($page, $parse=true){
		$page = PageCache::getPage($page);
		$html = "";
		
		if($parse){
			$html .= (string) $page;
			$html = $this->parser->parse($html);
			
		}else{ //read raw
			$html .= (string) $page;
		}
		
		// Check group rights
		if($this->loginController->isPermissiontoRead("admin")){} #Always all permissions
		else if($page->group() == "none"){} #Everyone has none rights
		else if($this->loginController->isPermissiontoRead($page->group()) ){} #Real check for groups
		else{	#Error message
			$user = $this->loginController->username();
			$html = "<p>User  has no permission to read page ". $page->name() ."</p><br/>";
		}
		
		return $html;
	}
	
	/**
	* Print form to edit page
	* @param string $pagename Name of page wanted to edit
	* @return string "Edit Page"-HTML
	*/
	function printEditPageForm($pagename){	
		$page = PageCache::getPage($pagename);
		$parse = $page->parse();
		
		if(!$parse){
			$parse = "";
		}else{
			$parse = "checked";
		}
		
		#Check right to modify
		if($GLOBALS['loginController']->isPermissiontoRead("admin", "w")){}
		else if(!$GLOBALS['loginController']->isPermissiontoRead("none", "w")){ return "<p>Not authorized.</p>"; }
		else if($GLOBALS['loginController']->isPermissiontoRead($page->group(), "w")){}
		else{return "<p>Not authorized.</p>";}
		
		//Group select
		$sh = "<select name='group' autofocus>";
		#$groups = $this->groupDB->getAllGroups();
		$groups = TGroup::getAllGroups();
		
		foreach($groups as $g){
			if($g == $page->group()){
				$sh .= "<option value='$g' selected='selected'>$g</option>";
			}else{
				$sh .= "<option value='$g'>$g</option>";
			}
		}
		$sh .= "</select>";
		
		if(!$GLOBALS['loginController']->isPermissiontoRead("admin")){
			$sh = "[Only for admins]";
		}
		
		$url = $GLOBALS['current_url'];
		$html = '
		<form name="editform" action="'.$url.'index.php?page=' . $pagename . '&action=write" method="post" > ' . "\n" . '
		<textarea name="pageedit" ><!-- EDIT_PLACEHOLDER --></textarea> ' . "\n" . '
		<input type="hidden" name="pagename" value=' . $pagename . ' /> ' . "\n" . '
		<br/><input type="checkbox" name="parse" value="1" '.$parse.'>Parse output</input><br/>
		'. "Group:" . $sh .'
		<input type="submit" value="Save" /> ' . "\n" . '
		</form>
		<br/>
		<a href="http://www.simplewiki.org/language">Syntax</a><br/>
		';
		
		
		
		if(isset($this->templateInterface)){
			$html = (string) $this->templateInterface->printEditPageForm($html);
		}
		
		$html = str_replace("<!-- EDIT_PLACEHOLDER -->", (string)$page, $html);
		
		return $html;
	}
	
	/**
	* Print navigation bar login form or logout link
	* @return string HTML string of navbar links
	* TODO: Change to use arrays
	*/
	public function navbarLoginTools(){
			$url = $GLOBALS['current_url'];
			$html = "";
			if($this->loginController->isLogged() ){
				$html .= "<a href='".$url."index.php?action=logout'>Logout [" . $_SESSION['username'] . "]</a>";
			}
			
			if(isset($this->templateInterface)){
				$html = (string) $this->templateInterface->navbarLoginTools($html);
			}
			return $html;
	}
	
	/**
	* print LoginForm for navbar
	*/
	public function navbarLoginForm(){
		$page = $GLOBALS['page'];
		$url = $GLOBALS['current_url_ssl'];
		$html = "";
		if(!$this->loginController->isLogged() && ($GLOBALS['sslConnection'] || $GLOBALS['overrideSSLCheck']) ){
				$html .= "<form class='navbar-form pull-right' action='".$url."index.php?action=read&page=$page' method='POST' >\n";
				$html .= "<input class='span1' type='text' name='username'>\n";
				$html .= "<input class='span1' type='password' name='password'>\n";
				#$html .= "<input type='submit' name='login_request' value='Login'>\n";
				$html .= '<button type="submit" name="login_request" class="btn btn-inverse">Login</button>' . "\n";
				$html .= "</form>\n";
			}
		return $html;	
	}
	
	/**
	* print openID LoginForm (button) for navbar
	*/
	public function navbarOpenIDLoginForm(){
		$page = $GLOBALS['page'];
		if(!$this->loginController->isLogged() ){
			$url = $GLOBALS['current_url'];		
			$html = "<form class='navbar-form pull-right' action='".$url."index.php?openidlogin_request=true&page=$page' method='POST' >\n";
			$html .= '<button type="submit" name="lr" class="btn btn-inverse">Google Login</button>' . "\n";
			$html .= "</form>\n";
			
			return $html;
		}
	}
	
	
	/**
	* Print links page tools (currently edit only)
	* @param string $page Name of page wanted to edit
	* @return string HTML string
	*/	
	function printPageTools($page){
		$html = array();
		if( $this->loginController->isLogged() ){ //This should be mode check
			$editlinks = array();
			$editlinks[$page]= "<a href=index.php?page=" . $page . "&action=edit>Edit page $page</a>";
			$editlinks[NAVBAR]= "<a href=index.php?page=" . NAVBAR . "&action=edit>Edit ".NAVBAR."</a>";
			$editlinks[SIDEBAR]= "<a href=index.php?page=" . SIDEBAR . "&action=edit>Edit ".SIDEBAR."</a>";
			
			$html["Edit"] = $editlinks;
			// History, page deleting, etc could be implemented
		}
		
		if(isset($this->templateInterface)){
			$html = (string) $this->templateInterface->printPageTools($html);
		}else{
			$t = "";
			foreach($html as $h){
				$t.= implode(" ", $h);
			}
			
			$html = $t;
		}
		
		return $html;
	}

	/**
	* Print links to admintools
	* @return string HTML string
	*/
	function printAdminTools(){
		$url = $GLOBALS['current_url'];
		$html = "";
		if( $this->loginController->isLogged() ){ //This should be mode check
//			$html .= "<a href='".$url."index.php?page=Site.menubar&action=edit'>Edit navbar</a>\n";
			$html .= "<a href='".$url."?page=Admin'>Admin tools</a>\n";
			// History, page deleting, etc could be implemented
		}
		
		if(isset($this->templateInterface)){
			$html = (string) $this->templateInterface->printAdminTools($html);
		}

		return $html;
	}
	
	/**
	* Print navbar by converting NAVBAR page to links
	*
	* NAVBAR page syntax one link per row, Syntax "PageName"(SEPARATOR "Title")
	* examples:
	* SomeRandomSite
	* SomeRandomSite|TitleToRandomSite
	*
	* @param string $page pageName
	* @param string $separator Separator symbol for NAVBAR page syntax
	* @return string HTML string of navbar links
	*/
	function printNavbar($page, $separator="|"){
		$p = pageCache::getPage($page);
		if(!$GLOBALS['loginController']->isPermissiontoRead($p->group(), "r")){
			return "";
		};
		
		$url = $GLOBALS['current_url'];
		
		$html = array();
		
		$navbar = $this->readPage($page, false);
		$navbar = explode("\n", $navbar);
				
		foreach ($navbar as $row){
			
			$pieces = explode($separator, $row);
			if(sizeof($pieces) == 1){
				$html[$row] = $url . "index.php?page=" . $row;
			}else if(sizeof($pieces) > 1){
				$html[ $pieces[1] ] = $url . "index.php?page=" . $pieces[0];
			}
		}
		
		if(isset($this->templateInterface)){
			$html = (string) $this->templateInterface->printNavbar($html);
		}else{
			$t = "";
			foreach($html as $k => $v){
				$t .= "<a href='$v'> $k </a>";
			}
			$html = $t;
		}
		
		return $html;
	}
	
	/**
	* Print last modification date of page
	* @param string $page Name of page which mdate is wanted
	* @return string HTML string
	*/
	function printPageLastModified($page){
		$page = PageCache::getPage($page);
		$html = "<p>Page last modified on ". $page->lastModified() ."</p>";
		
		if(isset($this->templateInterface)){
			$html = (string) $this->templateInterface->printPageLastModified($html);
		}
		
		return $html;
	}
	
	/**
	* Print small powered by thcms VERSION text
	* @return string HTML string
	*/
	function printPoweredByText(){
		//$html = "<p>Powered by <a href='https://bitbucket.org/thaapaniemi/thcms'>THCMS</a> ". VERSION ."</p>";
		$html = "<p>Powered by <a href='https://bitbucket.org/thaapaniemi/thcms'>THCMS</a> </p>";
		
		if(isset($this->templateInterface)){
			$html = (string) $this->templateInterface->printPoweredByText($html);
		}
		
		return $html;
	}	
	
	
	/**
	* Print pageNotFoundMessage
	*
	* @param string $page Page name
	* @return string Template-compatible html
	*/
	function printPageNotFoundMessage($page){
		$url = $GLOBALS['current_url'];
		$html = "<p>Page $page not found. " . "<a href='".$url."index.php?action=create&page=$page'>Create page $page?</a></p>";
		
		if(isset($this->templateInterface)){
			$html = (string) $this->templateInterface->printPageNotFoundMessage($html);
		}
		
		return $html;
	}

}
?>
