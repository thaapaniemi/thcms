<?php
/**
* TCHMS Key-Value DB class
* @package THCMS
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/


//////////

/**
* TCHMS Key-Value DB class for associative array storing

* @package THCMS\Core
*/
class KV{

	/**
	* Private constructor (for not making objects of it)
	*/
	private function __construct(){}


	/**
	* Get value from DB
	* Returns associative array associated to key.
	* @param string $key Key
	*
	* @return false if data not found
	* @return array data otherwise
	*/
	static public function get($key){
		try{
			$orm = ORM::for_table("keyvaluepairs")->where('id', $key)->find_one();
		}catch(Exception $e){ echo "KV: ORM get problem.<br/>\n"; echo $e->getMessage() . "<br/>\n"; return false; }
		
		if(!$orm){
			return false;
		}
		
		$value = $orm->value;
				
		$jsondata = json_decode($value);
		return $jsondata;
	}
	
	/**
	* Set value to DB
	*
	* Makes JSON from associative array and saves it to DB. There must be valueType defined in array.
	* @param string $key Key
	* @param array $value array
	* @param boolean $replace If exists, shall it be replaced?
	* @return boolean Success or not
	*/
	static public function set($key, $value, $replace=true){
		if(gettype($value) != 'array' || !isset($value['valueType'])){
			throw new InvalidArgumentException("KV: Value of key $key is not an array or valueType is not set.");
		}
		
		$jsondata = json_encode($value);
		
		try{
			$orm = ORM::for_table("keyvaluepairs")->where('id', $key)->find_one();
			if(!$orm){
				$orm = ORM::for_table("keyvaluepairs")->create();
			}else{
				if(!$replace){
					return false;
				}
			}
			$orm->id = $key;
			$orm->value = $jsondata;
			$orm->save();
			return true;
		}catch(Exception $e){ echo "KV: ORM set problem.<br/>\n"; echo $e->getMessage() . "<br/>\n"; return false; }
	}
	
	/**
	* Delete kv-pair from db
	* @param string $key Key to data
	*/
	static public function delete($key){
		try{
			$orm = ORM::for_table("keyvaluepairs")->where('id', $key)->find_one();
				
		if($orm){
			$orm->delete();
		}
		}catch(Exception $e){
			echo "KV: Delete problem. <br/>\n"; echo $e->getMessage() . "<br/>\n"; return false;
		}
	}

} // /KV

?>
