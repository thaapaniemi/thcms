<?php

/**
* TCHMS PageCache class declaration file
* @package THCMS\Core
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

//-----------------------------\\

/**
* TCHMS PageCache class declaration
* @package THCMS\Core
*
* PageCache keeps $max (currently hard-coded to 5) page objects in memory for lesser db use.
* Normally thcms loads 3 pages, WANTED, SIDEBAR and NAVBAR.
*/
class PageCache{
	
	/**
	* Static variables for max pages in memory.
	*/
	private static $max = 5;	
	
	/**
	* Static variables for array of pages.
	*/
	private static $pages = array();
	
	/**
	* GetPage method
	*
	* GetPage loads or creates page to static array and if there are over
	* $max pages it removes first one loaded (this operation saves it to db.)
	*
	* @param string $page Name of page wanted.
	* @return TPage wanted page (or new empty page).
	*/
	public static function getPage($page){
		if( ! isset(self::$pages[$page] ) ){
			self::$pages[$page] = TPage::open($page);
		}
		
		if(sizeof(self::$pages) > self::$max){
			$removeKey = array_keys(self::$pages)[0];
			unset( self::$pages[$removeKey] );
		}
		
		return self::$pages[$page];
	}

}


?>
