<?php
/**
* TCHMS UserDB class declaration file
* @package THCMS
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

require_once(BASEDIR . "/3rdparty/PasswordHash.php");
require_once(BASEDIR . '/3rdparty/idiorm/idiorm.php');

/**
* TUser class declaration
*
* THCMS User class for wrapping functionality for User-ORMClass
* @package THCMS\DB
*/
class TUser{

	/**
	* PasswordHash object
	*/
	private $hasher = null;

	/**
	* ORM User object
	*/
	private $user = null;
	
	/**
	* Do we want to save this user?
	*/
	private $ischanged = false;


	/**
	* Get all usernames from db
	* @return array Array of usernames
	*/
	static public function getAllUsers(){
	  try{
		$users0 = ORM::for_table(USERCLASS)->select('id')->order_by_asc('id')->find_many();
		$users = array();
		foreach($users0 as $u){
			$users[] = $u->id;
		}
		return $users;
	  }catch(Exception $e){
	  	echo "Tuser:getAllUsers error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; }
	}

	/**
	* Constructor
	* @param $username string Username
	* @param $password string Password
	*/
	public function __construct($username, $password=null){
	  try{
		$this->hasher = new PasswordHash(8, FALSE);
		$loadedUser = ORM::for_table(USERCLASS)->where('id', $username)->find_one();
		
		// When user doesn't exists we create one
		if(!$loadedUser){
			$this->user = ORM::for_table(USERCLASS)->create();
			$this->user->id = $username;
			
			if(isset($password)){
				$this->user->pw_hash = $this->hasher->HashPassword($password);
				$this->ischanged = true;
				
				$this->user->save();
				$this->setGroup("none", "r");
				
			}else{
				$this->user = null;
			}
		}else{
			$this->user = $loadedUser;
		}
		
	  }catch(Exception $e){
	  	echo "TUser:Constructor error.<br/>\n";  echo $e->getMessage() . "<br/>\n";
	  }
	}
	
	/**
	* Login with openID idemtity
	* @param string @openid openID identity
	* return TUser $user TUser object
	*/
	public static function loginWithOpenID($openid){
		try{
			$user = ORM::for_table(USERCLASS)->where('openid', $openid)->find_one();
			if($user){
				$user = new TUser($user->id);
			}
			
			return $user;
		}catch(Exception $e){
	  	echo "Tuser:loginWithOpenID error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; 
	  }
	}
	
	/**
	* Destructor used to save changes
	*/
	public function __destruct(){
		try{
			if(isset($this->user) && $this->ischanged){
				$this->user->save();
			}
		}catch(Exception $e){
	  	echo "TUser:destructor error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; 
	  }
	}
	
	/**
	* Authenticate user (check that password matches)
	* @param $password string password
	* @return boolean does password match
	*/
	public function authenticate($password){
		try{
			if(!isset($this->user)){
				return false;
			}
			return $this->hasher->CheckPassword($password, $this->user->pw_hash);
		}catch(Exception $e){
	  	echo "Tuser:authenticate error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; 
	  }
	}
	
	/**
	* ToString method
	*/
	public function __toString(){
		return $this->user->id;
	}
	
	/**
	* Check if user has permission to view group
	* @param $group string Group name
	* @param $rw string "r" or "w"
	* @return boolean permission
	*/
	public function isPermission($group, $rw="r"){
		if(in_array($group, $this->getGroups())){
			if($rw == "w"){
				return ($this->getGroup($group)->rw == $rw);
			}
			return true;
		}
		return false;
	}
	
	/**
	* get group-ORM object
	* @param string $group Name of group
	* @return ORM ORM-object;
	* @return null Null if not found
	*/
	private function getGroup($group){
		try{
			$g = ORM::for_table(GROUPCLASS)->where('user', $this->user->id)->where('group', $group)->find_one();
			if($g){
				return $g;
			}
			return null;
		}catch(Exception $e){
	  	echo "Tuser:getGroup error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; 
	  }
	}
	
	/**
	* Get all groups associated to user
	* @return array of groups
	*/
	public function getGroups(){
		try{
			$groups = ORM::for_table(GROUPCLASS)->where('user', $this->user->id)->find_many();
			
			$temp = array();
			foreach($groups as $g){
				$tempgroup = new TGroup(null,null, null,$g);
				$temp[] = $tempgroup;
			}
			
			return $temp;		
		}catch(Exception $e){
	  	echo "Tuser:getGroups error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; 
	  }
	}
	
	/**
	* Set group to user
	* @param $group string Name of group
	* @param $rw string "r" for readOnly, "w" for read/write
	*/
	public function setGroup($group, $rw){
		try{
			$new = new TGroup($this->user->id, $group, $rw);
			$new->save();
		}catch(Exception $e){
	  	echo "Tuser:setGroup error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; 
	  }
	}
	
	/**
	* Delete user-group association
	* @param $group string Name of group
	*/
	public function unsetGroup($group){
		try{
			$target = new TGroup($this->user->id, $group);
			if($target){
				$target->delete();
			}
		}catch(Exception $e){
	  	echo "Tuser:unsetGroup error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; 
	  }
	}
	
	/**
	* Combined Getter/Setter for password
	* @param $newpw string New password
	* @return Hash of password
	*/
	public function password($newpw = null){
		if(isset($newpw)){
			$this->user->pw_hash = $this->hasher->HashPassword($newpw);
			$this->ischanged = true;
		}
		
		return $this->user->pw_hash;
		
	}
	
	/**
	* Combined Getter/Setter for openID identity
	* @param $new string New identity
	* @return OpenID identity
	*/
	public function openid($new = null){
		if(isset($new)){
			$this->user->openid = $new;
			$this->ischanged = true;
			#$this->user->save();
		}
		
		return $this->user->openid;
	}
	
	/**
	* Remove openID identity from user
	*/
	public function removeOpenid(){
		$this->user->openid = null;
		$this->ischanged = true;
	}
	
	
	
	/**
	* Create temporary identifier for password hash.
	* @param $length int length of wanted id, value range (1...32)
	* @return string id
	*/
	public function pwhash($length=4){
		$min = 31-$length;
		$salt = "DxAsUIoMpuFxtMW0ohCHOoiCdHAJruky" . session_id(); // little extra security
		return substr(md5($this->user->pw_hash . $salt), $min,31); // this should work pretty good for not revealing the real hash
	}
	
	/**
	* Delete user and all groups associated
	*/
	public function delete(){
		try{
			TGroup::deleteAllGroupsOfUser($this->user->id);
			
			$this->user->delete();
			$this->user = null;
		}catch(Exception $e){
	  	echo "Tuser:delete error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; 
	  }
	}
	
	/**
	* Create table users to db
	* For installation purposes
	*/
	static public function createTables(){
		try{
			$db = ORM::get_db();
			$sql= "CREATE TABLE IF NOT EXISTS ". USERCLASS." (
				  `id` varchar(255) NOT NULL DEFAULT '',
				  `pw_hash` varchar(255) DEFAULT NULL,
				  `opeenid` varchar(255) DEFAULT NULL,
				  PRIMARY KEY (`id`)
				  );";
			$ss = $db->prepare($sql);
			$ss->execute();
		}catch(Exception $e){
	  	echo "Tuser:createTables error.<br/>\n";  echo $e->getMessage() . "<br/>\n"; return false; 
	  }
	}
	
}


?>
