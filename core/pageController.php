<?php
/**
* TCHMS pageController class declaration file
* @package THCMS\Core
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

////.....

/**
* TCHMS pageController class declaration
*
* @package THCMS\Core
*/
class PageController{

	/**
	* Constructor
	*/
	function __construct() {}
	
	/**
	* Destructor
	*/
	function __destruct() {}

	/**
	* Update page content
	* @param string $pagename Page name
	* @param string $content New content of page
	* @param boolean $parse Parse output?
	* @param string $group Page group
	*/
	function updatePage($pagename, $content, $parse, $group="none"){
		$page = PageCache::getPage($pagename);
		#Check right to modify
		if($GLOBALS['loginController']->isPermissiontoRead("admin")){}
		else if($GLOBALS['loginController']->isPermissiontoRead("none")){ echo "<p>Not authorized.</p>"; return; }
		else if($GLOBALS['loginController']->isPermissiontoRead($page->group())){}
		else{echo "<p>Not authorized.</p>"; return;}
	
		$page->content($content);
		$page->parse($parse);
		$page->lastModified( date("Y-m-d H:i:s") );
		$page->group($group);
		//$this->pageDB->writePage($pagename, $content, $parse);
		$page = null;
	}
	
	/**
	* Create new page
	* @param string $pagename Name of new page
	*/
	function createPage($pagename){
			$page = PageCache::getPage($pagename);
			if($page->getIsNew()){
				$page->setChange(true);
				return true;
			}else{
				return false;
			}
	}

}//endofclass

?>
