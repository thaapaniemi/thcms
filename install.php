<?php

/**
* TCHMS installation file.
* @package THCMS
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
* Edit config.php. Run this file once to do necessary db creations for getting
* working thcms installation.
*/

require_once("config.php");

$html = "";

try{
$html .= "Creating users db tables.<br/>\n";
TUser::createTables();
}catch(PDOException $e){$html .= " Creating usertable failed. <br/><br/>\n\n";}

try{
$html .= "Creating pages db tables.<br/>\n";
TPage::createTables();
}catch(PDOException $e){$html .= " Creating pagetable failed. <br/><br/>\n\n";}

try{
$html .= "Creating groups db tables.<br/>\n";
TGroup::createTables();
}catch(PDOException $e){$html .= " Creating pagetable failed. <br/><br/>\n\n";}


try{
$html .= "Creating default user.<br/>\n";
//Create default user to db, admin:admin
#$userDB->createNewUser("admin", "admin", "admin");
$none = new TUser("none","");
$none->setGroup("none","r");

$admin = new TUser("admin","admin");
$admin->setGroup("none","r");
$admin->setGroup("admin","w");


}catch(PDOException $e){$html .= " Creating new user failed. <br/><br/>\n\n";}

$html .= "Creating default pages.<br/><br/>\n\n";

try{
TPage::open(MAINPAGE, true);
}catch(PDOException $e){$html .= MAINPAGE. " already exists" . "<br/><br/>\n\n";}


try{
$adminpage = TPage::open(ADMINPAGE, true);
$adminpage->parse(true);
$adminpage->content("::[admin]::");
$adminpage->group("admin");
}catch(PDOException $e){$html .= ADMINPAGE. " already exists" . "<br/><br/>\n\n";}


try{
$nav = TPage::open(NAVBAR, true);
$nav->group("none");
$nav->parse("false");

}catch(PDOException $e){$html .= NAVBAR. " already exists" . "<br/><br/>\n\n";}

try{
$adminbar = TPage::open(ADMINBAR, true);
$adminbar->parse(false);
$adminbar->content("Admin");
$adminbar->group("admin");

}catch(PDOException $e){$html .= ADMINBAR. " already exists" . "<br/><br/>\n\n";}


try{
TPage::open(SIDEBAR, true);
}catch(PDOException $e){$html .= SIDEBAR. " already exists" . "<br/><br/>\n\n";}

$html .= "Done. <a href='index.php'>To Main page</a><br/>\n";

THTP::addReplace('<!-- THCMS:CONTENT -->', $html);

echo THTP::getHTML();

?>
