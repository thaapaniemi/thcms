<?php

/**
* TCHMS Default Twitter Bootstrap Admin Interface file.
*
* Here are those methods what generates admin layout-dependant html
* @package THCMS\template
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*
*/

//////////

/**
* TCHMS Default Twitter Bootstrap Admin Interface class
* @package THCMS\template
*/
class TemplateAdminInterface{

	/**
	* Template modifications to listAllPages
	* @param string $html Input HTML
	* @return string Output HTML
	*/
	function listAllPages($html){
		return $html;
	}
	
	/**
	* Template modifications to deleteConfirmation
	* @param string $html Input HTML
	* @return string Output HTML
	*/
	function deleteConfirmation($html){
		return $html;
	}


	/**
	* Template modifications to generatePageLinks
	* @param string $html Input HTML
	* @return string Output HTML
	*/
	function generatePageLinks($html){
		return $html;
	}
	
	/**
	* Template modifications to listAllUsers
	* @param string $html Input HTML
	* @return string Output HTML
	*/
	function listAllUsers($html){
		return $html;
	}
	
	/**
	* Template modifications to changePasswordForm
	* @param string $html Input HTML
	* @return string Output HTML
	*/
	function changePasswordForm($html){
	
		$dom = new simple_html_dom();
		$dom->load($html);
		
		foreach($dom->find('input') as $a ){
			if($a->name == "pw"){
				$a->class = "span2";
			}
			
			if($a->type == "submit"){
				$a->class = "btn btn-small btn-inverse";
			}
			
		}
		
		return (string)$dom;
	}

	/**
	* Template modifications to showUserCreationForm
	* @param string $html Input HTML
	* @return string Output HTML
	*/
	function showUserCreationForm($html){
		$dom = new simple_html_dom();
		$dom->load($html);
		
		foreach($dom->find('input') as $a ){
			if($a->name == "pw" || $a->name == "username"){
				$a->class = "span2";
			}
			
			if($a->type == "submit"){
				$a->class = "btn btn-small btn-inverse";
			}
			
		}
		
		return (string)$dom;
	}
	
	/**
	* Template modifications to deleteUserConfirmation
	* @param string $html Input HTML
	* @return string Output HTML
	*/
	function deleteUserConfirmation($html){
		return $html;
	}
	
	/**
	* Template modifications to printPWChangeSuccessful
	* @param string $html Input HTML
	* @return string Output HTML
	*/
	function printPWChangeSuccessful($html){
		return $html;
	}
	
	/**
	* Template modifications to generatePWChangeLink
	* @param string $html Input HTML
	* @return string Output HTML
	*/
	function generatePWChangeLink($html){
		return $html;
	}
	
	/**
	* Template modifications to createUserLink
	* @param string $html Input HTML
	* @return string Output HTML
	*/
	function createUserLink($html){
		return $html;
	}
	
	
}

?>
