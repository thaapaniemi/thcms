#!/bin/bash

# Script for downloading/updating to newest Twitter Bootstrap

rm -rf ./bootstrap
wget http://twitter.github.com/bootstrap/assets/bootstrap.zip
unzip bootstrap.zip
rm bootstrap.zip
chmod 755 -R bootstrap
