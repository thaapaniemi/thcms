<?php 

/**
* TCHMS Default Twitter Bootstrap Interface file.
*
* Here are those methods what generates layout-dependant html
* @package THCMS\template
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*
*/

//////////

/**
* TCHMS Default Twitter Bootstrap Interface class
* @package THCMS\template
*/
class TemplateInterface{

	/**
	*	Logged in status
	*/
	private $isLogged = false;


	/**
	* Sets the logged in status to templateInterface 
	*
	* @param boolean $isLoggedOrNot is user logged in?
	*/
	function setLoggedInStatus($isLoggedOrNot){
		$this->isLogged = $isLoggedOrNot;
	}
	
	/**
	* Print navigation bar login form or logout link.
	* @param string $html Input HTML string
	* @return string HTML string of navbar links
	*/
	function navbarLoginTools($html){
			$dom = new simple_html_dom();
			$dom->load($html);

			//make links to listslinks
			foreach($dom->find('a') as $a ){
				$a->outertext = '<li>' . $a->outertext . '</li>';
			}
			
			foreach($dom->find('form') as $form ){
				$form->class = 'navbar-form pull-right';
			}
			
			//make form inputs to nicer
			foreach($dom->find('input') as $input ){
				if($input->type == 'text' || $input->type == 'password'){
					$input->class = 'span1';
				}else if($input->type == 'submit'){
					$input->class = 'btn btn-small btn-inverse';
				}
			}
			
			
		return $dom;
	}

	/**
	* Print links page tools (currently edit only)
	* @param array $linkarray tool links in nested array array( $mainname => array($id => $url) )
	* @return string HTML string
	*/	
	function printPageTools($linkarray){
		
		$html = "";
		
		// Main level Links
		// TODO: Fix this qnd: Better support for nesting
		foreach($linkarray as $mk=>$mv){
			foreach($mv as $k=>$v){
				$html .= "<li> $v </li>\n";
				break;
			}
		}
			
		return $html;		
	}
	
	/**
	* Print links to admintools
	* @param string $html Input HTML string
	* @return string HTML string
	*/
	function printAdminTools($html){
		
		$dom = new simple_html_dom();
		$dom->load($html);
		
		//make links to listslinks
		foreach($dom->find('a') as $a ){
			$a->outertext = '<li>' . $a->outertext . '</li>';
		}
		
		return $dom;
	}
	
	/**
	* Navbar template modifications
	* @param array $navlinks Links in array($name=>$url)
	* return string HTML output
	*/
	function printNavbar( $navlinks ){
		$html = "";
		
		//make links to listslinks
		foreach($navlinks as $name=>$url){
			$html .= "<li><a href='$url'>$name</a></li>\n";
		}
		
		return $html;
	}
	
	/**
	* Print small powered by thcms VERSION text
	* @param string $html Input HTML string
	* @return string HTML string
	*/
	function printPoweredByText($html){
		$dom = new simple_html_dom();
		$dom->load($html);
		
		//make links to listslinks
		foreach($dom->find('p') as $p ){
			$p->innertext = "<span style='font-size:66%'>" . $p->innertext . "</span>" ;
			$p->outertext .= "<br/>\n";
		}
		
		return $dom;
	}
	
	/**
	* Print last modification date of page
	* @param string $html Input HTML string
	* @return string HTML string
	*/
	function printPageLastModified($html){
		$dom = new simple_html_dom();
		$dom->load($html);
		
		//make links to listslinks
		foreach($dom->find('p') as $p ){
			$p->innertext = "<span style='font-size:66%'>" . $p->innertext . "</span>" ;
			$p->outertext .= "<br/>\n";
		}
		
		return $dom;
	}
	
	/**
	* Print pageNotFoundMessage
	* @param string $html Input HTML string
	* @return string template-compatible html
	*/
	function printPageNotFoundMessage($html){
		return $html;
	}
	
	/**
	* Print form to edit page
	* @param string $html Input HTML string
	* @return string template-compatible html
	*/
	function printEditPageForm($html){	
		$dom = new simple_html_dom();
		$dom->load($html);
		
		foreach($dom->find('input') as $input ){
			if($input->type == "submit"){
				$input->class = "btn";
				
				$input->outertext = "\n<br/><br/>\n" . $input->outertext;
			}
		}
		
		foreach($dom->find('textarea') as $ta ){
			$ta->rows = 23;
		}
		
		return $dom;
	}
	
}

?>
