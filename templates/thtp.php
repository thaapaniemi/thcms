<?php

/**
* TH (Simple) Template Processor
* @package THTP
*
* @author Tomi Haapaniemi tomi.haapaniemi@metropolia.fi
* @license BSD
* @license http://opensource.org/licenses/BSD-3-Clause
*
*/

/**
* Class declaration for THTP
* @package THTP
*/
class THTP{

	/**
	* template HTML string
	*/
	protected static $template = null;

	/**
	* template replaces array
	* format [String what to replace] => [String replace]
	*/
	protected static $replaces = array();


	/**
	* Constructor, This is static only class
	*/
	private function __construct(){}

	/**
	* Load template to engine
	* @param string $template HTML code of template
	*/
	public static function load($template){
		THTP::$template = $template;
	}

	/**
	* Add replace to template
	*
	* @param string $codeword String what is replaced
	* @param string $newReplace String replacing codeword
	*/
	public static function addReplace($codeword, $newReplace){
		if(!isset(THTP::$template)){
			throw new RuntimeException("Template not loaded");
		}
		
		THTP::$replaces[$codeword] = $newReplace;
	}

	/**
	* Do Changes to template to get final html output 
	*
	* @return string HTML generated from template
	*/
	public static function getHTML(){
		if(!isset(THTP::$template)){
			throw new RuntimeException("Template not loaded");
		}
	
		$a = THTP::$template;
	
		foreach(THTP::$replaces as $k=>$v){
			$a = str_replace($k,$v,$a);
		}
	
		return $a;
	}
}

?>
